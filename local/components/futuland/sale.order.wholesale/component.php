<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if( !CModule::IncludeModule("iblock") || !CModule::IncludeModule('catalog') || !CModule::IncludeModule("nkhost.phpexcel") )
	return;
use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\PaySystem
    //Bitrix\Sale\Delivery,
    //Bitrix\Catalog,
    //Bitrix\Catalog\ProductTable,
    //Bitrix\Iblock\ElementTable,
    ;

global $USER;
global $PHPEXCELPATH;

//BXClearCache(true, "/personal/orderload/");

require_once ($PHPEXCELPATH . '/PHPExcel/IOFactory.php');
$PRODUCT_IBLOCK_ID = (!empty($arParams['IBLOCK_ID'])) ? $arParams['IBLOCK_ID'] :211;
$SKU_IBLOCK_ID = (!empty($arParams['SKU_IBLOCK_ID'])) ? $arParams['SKU_IBLOCK_ID'] :212;

if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter)))//, $pagerParameters
{
    $tree = CIBlockSection::GetTreeList(
	    $arFilter=Array('IBLOCK_ID' => $PRODUCT_IBLOCK_ID),
	    $arSelect=Array()
	);
	while($section = $tree->GetNext()) {
		$arResult['SECTIONS'][] = $section['ID'];
	}
	
	$this->setResultCacheKeys(array('SECTIONS'));

	$this->includeComponentTemplate();
}

$sections = Context::getCurrent()->getRequest()->get("SECTIONS");


if( (!empty($sections) && $arParams['TYPE'] == 'ORDERLOAD') || $arParams['TYPE'] == 'PRICE') {

    $cache = Bitrix\Main\Data\Cache::createInstance();
    if ($cache->initCache(86400, "wsb-arElements", "wsb/orderload"))
    {
        $arElements = $cache->getVars();
        //dfile($arElements);
    }
    elseif ($cache->startDataCache())
    {
        $arSelect = Array("ID", "NAME", "PROPERTY_CML2_ARTICLE", "CATALOG_TYPE", "IBLOCK_SECTION_ID");
        $arFilter = Array("IBLOCK_ID"=>IntVal($PRODUCT_IBLOCK_ID), "ACTIVE"=>"Y","INCLUDE_SUBSECTIONS"=>"Y");
        if($arParams['TYPE'] == 'ORDERLOAD')
            $arFilter['SECTION_ID'] = $sections;
        else
            $arFilter['SECTION_ID'] = $arResult['SECTIONS'];
        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $arElements = array();
        $tmpArSection = [];
        $arOffersIds = array();
        while($ar_fields = $res->fetch()) {
            $price = CCatalogProduct::GetOptimalPrice($ar_fields['ID'], 1, $USER->GetUserGroupArray(), 'N');

            $item = [
                'ID' => $ar_fields['ID'],
                'NAME' => $ar_fields['NAME'],
                'CML2_ARTICLE' => $ar_fields['PROPERTY_CML2_ARTICLE_VALUE'],
                'PRICE' =>  $price['PRICE']['PRICE'],
                'IBLOCK_SECTION_ID' => $ar_fields['IBLOCK_SECTION_ID']
            ];

            if($ar_fields['CATALOG_TYPE'] == 1)
                $arElements[] = $item;
            elseif($ar_fields['CATALOG_TYPE'] == 3){
                $arOffersIds[] = intval($item['ID']);
                $tmpArSection[$ar_fields['ID']] = $ar_fields['IBLOCK_SECTION_ID'];
            }
        }

        if(!empty($arOffersIds)) {
            $offersFilter = array('ACTIVE' => 'Y');
            $offers = CCatalogSKU::getOffersList(
                $arOffersIds,
                0,
                $offersFilter,
                array('NAME'),
                array()
            );
            //dfile($offers);
            foreach($offers as $elems) {
                foreach($elems as $elem) {

                    $price = CCatalogProduct::GetOptimalPrice($elem['ID'], 1, $USER->GetUserGroupArray(), 'N');
                    $item = [
                        'ID' => $elem['ID'],
                        'NAME' => $elem['NAME'],
                        'PRICE' =>  $price['PRICE']['PRICE'],
                        'IBLOCK_SECTION_ID' => $tmpArSection[$elem['PARENT_ID']]
                    ];
                    $arElements[] = $item;
                }
            }
        }
//dfile($tmpArSection);
//dfile($arElements);

        $cache->endDataCache($arElements);
    }


	require_once($PHPEXCELPATH.'/PHPExcel.php');
	require_once($PHPEXCELPATH.'/PHPExcel/Writer/Excel5.php');
	
	$xls = new PHPExcel();
	$xls->setActiveSheetIndex(0);
	$sheet = $xls->getActiveSheet();
	$sheet->setTitle('Прайслист');
	
	$sheet->setCellValue("A1", 'Название');
	$sheet->setCellValue("B1", 'Артикул');

    //$sheet->setCellValue("C1", 'Доступное количество');
    $sheet->setCellValue("C1", 'Количество к заказу');
    $sheet->setCellValue("D1", 'Цена');
    $sheet->setCellValue("E1", 'Стоимость');
    $sheet->setCellValue("F1", 'Внутренний ID');


	$sheet->getStyle('A1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('ff0000');
	$sheet->getStyle('B1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('B1')->getFill()->getStartColor()->setRGB('ff0000');
	$sheet->getStyle('C1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('C1')->getFill()->getStartColor()->setRGB('ff0000');
	$sheet->getStyle('D1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('D1')->getFill()->getStartColor()->setRGB('ff0000');
    $sheet->getStyle('E1')->getFill()->setFillType(
        PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('E1')->getFill()->getStartColor()->setRGB('ff0000');
    $sheet->getStyle('F1')->getFill()->setFillType(
        PHPExcel_Style_Fill::FILL_SOLID);
    $sheet->getStyle('F1')->getFill()->getStartColor()->setRGB('ff0000');


	//$sheet->getColumnDimension('A')->setAutoSize(true);
    $sheet->getColumnDimension('A')->setWidth(80);
	$sheet->getColumnDimension('B')->setAutoSize(true);
	$sheet->getColumnDimension('C')->setAutoSize(true);
	$sheet->getColumnDimension('D')->setAutoSize(true);
    $sheet->getColumnDimension('E')->setAutoSize(true);
    $sheet->getColumnDimension('F')->setAutoSize(true);
    //$sheet->getColumnDimension('G')->setAutoSize(true);

    //dfile($arElements);
    $i = 0;
    foreach ($arElements as $k=>$v) {
        // удаляем элементы которых нет в $sections при выборке
        if($arParams['TYPE'] == 'ORDERLOAD' && !in_array($v['IBLOCK_SECTION_ID'], $sections)) continue;

        $sheet->setCellValueByColumnAndRow(0, $i+2, $v['NAME']);
        $sheet->setCellValueByColumnAndRow(1, $i+2, $v['CML2_ARTICLE']);

        $sheet->setCellValueByColumnAndRow(2, $i+2, 0);
        $sheet->setCellValueByColumnAndRow(3, $i+2, $v['PRICE']);
        $sheet->setCellValueByColumnAndRow(4, $i+2,'=C'.($i+2).'*D'.($i+2));
        $sheet->setCellValueByColumnAndRow(5, $i+2, $v['ID']);
        $i++;
    }
    /*
	for ($i=0; $i < count($arElements) ; $i++) {
	    // удаляем элементы которых нет в $sections при выборке
        if($arParams['TYPE'] == 'ORDERLOAD' && !in_array($arElements[$i]['IBLOCK_SECTION_ID'], $sections)) continue;

		$sheet->setCellValueByColumnAndRow(0, $i+2, $arElements[$i]['NAME']);
		$sheet->setCellValueByColumnAndRow(1, $i+2, $arElements[$i]['CML2_ARTICLE']);

        $sheet->setCellValueByColumnAndRow(2, $i+2, 0);
        $sheet->setCellValueByColumnAndRow(3, $i+2, $arElements[$i]['PRICE']);
        $sheet->setCellValueByColumnAndRow(4, $i+2,'=C'.($i+2).'*D'.($i+2));
        $sheet->setCellValueByColumnAndRow(5, $i+2, $arElements[$i]['ID']);
	}
	*/
	//вывод общей стоимости заказа
	$sheet->setCellValueByColumnAndRow(0, $i+4, 'Полная стоимость заказа');
	$sheet->setCellValueByColumnAndRow(1, $i+4,'=SUM(E2:E'.($i+1).')');
	
	header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
	header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
	header ( "Cache-Control: no-cache, must-revalidate" );
	header ( "Pragma: no-cache" );
	header ( "Content-type: application/vnd.ms-excel" );
	if($arParams['TYPE'] == 'ORDERLOAD')
		header ( "Content-Disposition: attachment; filename=orderload.xls" );
	else
		header ( "Content-Disposition: attachment; filename=pricelist.xls" );
	ob_end_clean();
	// Выводим содержимое файла
	$objWriter = new PHPExcel_Writer_Excel5($xls);
	$objWriter->save('php://output');
	exit;
}

//$fid = CFile::SaveFile($_FILES['file'], "orders");
//$filePath = CFile::GetPath($fid);

if(is_uploaded_file($_FILES['file']['tmp_name'])){
    $file = $_FILES['file']['tmp_name'];

    $xls = PHPExcel_IOFactory::load($file);
// Устанавливаем индекс активного листа
    $xls->setActiveSheetIndex(0);
// Получаем активный лист
    $sheet = $xls->getActiveSheet();

    for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {
        $nColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
        for ($j = 0; $j < $nColumn; $j++) {
            $arProducts[$i][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
        }
    }

//проверка не пустой ли заказ и отсеивание количество к заказу = 0
    $emptyOrder = true;
    foreach( $arProducts as $k=>$v ) {
        if($v[2] == 0) {
            unset($arProducts[$k]);
        }elseif ($v[2] > 0){
            $emptyOrder = false;
        }
    }

    $text_result = '';

    if($emptyOrder){
        $text_result = "Загружен пустой или невалидный Excel!";
    }else{
        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        $order = Order::create($siteId, $USER->GetID());
        $order->setPersonTypeId(1);
        $order->setField('CURRENCY', $currencyCode);

        $propertyCollection = $order->getPropertyCollection();
        $somePropValue = $propertyCollection->getItemByOrderPropertyId(216);
        $somePropValue->setValue("Оптовый");

        //устанавливаем свойства
        $name = $USER->GetFullName();
        $email = $USER->GetEmail();
        $propertyCollection = $order->getPropertyCollection();
        $phoneProp = $propertyCollection->getPhone();
        $phoneProp->setValue($email);
        $nameProp = $propertyCollection->getPayerName();
        $nameProp->setValue($name);

        //создаем корзину
        $basket = Basket::create($siteId);
        //добавление в корзину товаров
        foreach($arProducts as $k=>$v){
            $item = $basket->createItem('catalog', $v[5]);
            $item->setFields(array(
                'QUANTITY' => $v[2],
                'CURRENCY' => $currencyCode,
                'LID' => $siteId,
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
        }


        $price = $basket->getPrice();
        if($price == 0) {
            $text_result = "Данного количества нет на складе 1";
        }elseif($price > 0 && $price <50000){
            $text_result = "Сумма заказа меньше 50.000 рублей!";
        }else{
            //привязываем корзину к новому заказу
            $order->setBasket($basket);

            // Создаём одну отгрузку и устанавливаем способ доставки - "Самовывоз" (он служебный)
            $shipmentCollection = $order->getShipmentCollection();
            $shipment = $shipmentCollection->createItem(
                \Bitrix\Sale\Delivery\Services\Manager::getObjectById(2)
            );
            $shipmentItemCollection = $shipment->getShipmentItemCollection();

            // Создаём оплату со способом #1 - наличные
            $paymentCollection = $order->getPaymentCollection();
            $payment = $paymentCollection->createItem();
            $paySystemService = PaySystem\Manager::getObjectById(1);
            $remainingSum = $order->getPrice() - $paymentCollection->getSum();
            $payment->setFields(array(
                'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
                'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
                'SUM' => $remainingSum
            ));

            //сохраняем
            $order->doFinalAction(true);
            $result = $order->save();
            $orderId = $order->getId();
        }

    }

    if($orderId > 0){
        LocalRedirect('/personal/orders/OPT-'.$orderId."/");
    } else echo "<h2 class='text-danger'>".$text_result."</h2>";

}

