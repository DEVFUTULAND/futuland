<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<?
$this->addExternalJS(SITE_TEMPLATE_PATH.'/js/bootstrap-checkbox-tree.js');
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-checkbox-tree.js');?>
<h6 class="text-danger">Минимальная сумма оптового заказа - 50.000 рублей.</h6>
<h3>Для загрузки заказа через EXCEL файл, пожалуйста, выполните следующие шаги:</h3>
<div class="col-xs-6">
<h4>Шаг 1: Выберите интересующие Вас категории товаров, и нажмите "Скачать шаблон"</h4>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"sections_list_orderload",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		"SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
		"SECTIONS_LIST_PREVIEW_PROPERTY" => $arParams["SECTIONS_LIST_PREVIEW_PROPERTY"],
		"SECTIONS_LIST_PREVIEW_DESCRIPTION" => $arParams["SECTIONS_LIST_PREVIEW_DESCRIPTION"],
		"SHOW_SECTION_LIST_PICTURES" => "N",
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],		
	),
	$component
);?>
</div>
<div class="step2 col-xs-6" style="">
<div class="floating">
<h4>Шаг 2: Загрузите заполненный шаблон файла EXCEL.<br>
    Будет автоматически сформирован заказ со способом доставки <span class="text-success">Самовывоз</span> и вариантом оплаты <span class="text-success">Наличные</span></h4>
<form name="order" method="POST" action="" enctype="multipart/form-data">
	<input type="file" name="file" value="" ><br>
	<button type="submit" class="btn btn-default">Отправить</button>
</form>
</div>
</div>
<script> 
 $(function(){
 var topPos = 500; //$('.floating').offset().top; //topPos - это значение от верха блока до окна браузера
 $(window).scroll(function() { 
  var top = $(document).scrollTop();
  var bottomPos = ($('.top_block').offset().top - 350);
  if (top > topPos) $('.floating').addClass('fixed'); 
  else $('.floating').removeClass('fixed');
  if (top > bottomPos) $('.floating').removeClass('fixed').css({ "position": "absolute","top": (bottomPos - 500) + "px" });
  
 });
	});
</script>
<style>
.floating {
margin: 0 auto;

}
.fixed {
 position: fixed;
 top: 35px; /*здесь указываем отступ сверху*/
 z-index: 9999; /*устанавливаем блок поверх всех элементов на странице*/
 width: 25%;
}
</style>
<?/*
<h2>Выбор разделов</h2>

	<form name="sections" method="POST" action="">
		<ul class="checkbox-tree root">
		<?$currentLevel = 1;?>
		<?foreach($arResult['SECTIONS'] as $key => $section):?>

			<li>
			
			<?if ($section["DEPTH_LEVEL"] < $arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"]):?>
				<ul>
			<?endif;?>
			
			<input id="<?=$section["ID"];?>" 
				class="checkbox" 
				type="checkbox" 
				name="SECTIONS[]" 
				value="<?=$section["ID"];?>"
				<?(in_array($section["ID"], $_REQUEST["SECTIONS"])) ? 'checked' : '' ?> />
			<label for="<?=$section["ID"];?>"><?=$section["NAME"]?></label>

			<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] < $section["DEPTH_LEVEL"] ):?>
				<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] - $section["DEPTH_LEVEL"] == 2):?>
					</ul>
				<?endif;?>
				</ul>
			<?endif;?>			
			</li>			
			<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] - $section["DEPTH_LEVEL"] == 1 || count($arResult['SECTIONS']) == $key+1):?>
				</li>
			<?endif;?>
		<?endforeach;?>
		</ul>
		<button type="submit">Отправить</button>
	</form>
*/?>