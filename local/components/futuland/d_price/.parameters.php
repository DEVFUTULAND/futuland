<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;

Loader::includeModule("iblock");

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"PRODUCT_ID" => array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => "PRODUCT_ID",
			"TYPE" => "STRING",
			"VALUES" => "",
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
        "CACHE_TIME" => array("DEFAULT"=>"36000000","TYPE"=>"A"),
	)
);