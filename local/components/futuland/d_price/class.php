<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc, Bitrix\Main\Loader;


Loc::loadMessages(__FILE__);

final class DPrice extends CBitrixComponent
{
    protected $geo;
    protected $product_id;
    protected $weight;
    protected $price;

    public function executeComponent(){

        $this->geo = $_SESSION['FUTU']["REGION"];
        $this->product_id = $this->arParams["PRODUCT_ID"];
        $this->weight = $this->arParams["WEIGHT"];
        $this->price = $this->arParams["PRICE"];

        if($this->startResultCache($this->arParams["CACHE_TIME"], SITE_ID."-".$this->product_id."-".$this->geo['LOCATION']['VALUE'],"delivery-product/".$this->product_id."/".$this->geo['LOCATION']['VALUE']))
        {
            $this->arResult = $this->makeVirtualOrder();

            $this->includeComponentTemplate();
        }
        return $this->arResult;
    }

    protected function makeVirtualOrder()
    {
        Loader::includeModule('sale');

        // 1 - создаем корзину и заказ
        $order = \Bitrix\Sale\Order::create(SITE_ID, 1);
        $basket = \Bitrix\Sale\Basket::create(SITE_ID);
        $item = $basket->createItem('catalog', $this->product_id);
        $item->setFields(array(
            'QUANTITY' => 1,
            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
            'LID' => SITE_ID,
            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            'WEIGHT' => $this->weight,
            'PRICE' => $this->price
        ));
        $order->setBasket($basket); // привязываем корзину к заказу
        $order->setPersonTypeId(1);  //ставим тип плательщика, чтобы пройти ограничения доставки по типу плательщика корректно

        // 2 - создаем объект отгрузки
        $shipmentCollection = $order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $shipment->setFields(array(
            'CURRENCY' => $order->getCurrency()
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();

        foreach ($order->getBasket() as $item) {
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }

        // 3 - расчет стоимости доставки до города
        $propertyCollection = $order->getPropertyCollection();//получаем коллекцию свойств заказа
        $property = $propertyCollection->getDeliveryLocation();//выбираем ту что отвечает за местоположение
        $property->setValue($this->geo['BX_LOC_CODE']);//передаем местоположение - москва - 0000073738
        //Далее получаем список доступных для данного местоположения доставок.
        $deliveries = Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($shipment);

        $arDeliveries = array();

        foreach ($deliveries as $key => $deliveryObj) {
            $arDelivery = [];
            $arDelivery['name'] = $deliveryObj->getNameWithParent();
            $arDelivery['description'] = $deliveryObj->getDescription();
            $arDelivery['logo_path'] = $deliveryObj->getLogotipPath();
            if($key == 95 || $key == 96){
                $calcResult = $deliveryObj->calculate($shipment);
                if ($calcResult->isSuccess()){
                    $arDelivery['price']['MAIN']['PRICE'] = $calcResult->getPrice();
                    $arDelivery['price']['MAIN']['PERIOD'] = $calcResult->getPeriodDescription();
                }
            } else
                $arDelivery['price'] = $deliveryObj->getConfigValues();

            $arDeliveries[$key] = $arDelivery;
        }
        unset($order,$shipment,$shipmentCollection,$propertyCollection);
        return $arDeliveries;
    }
}