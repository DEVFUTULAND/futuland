<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult)){?>
    <div id="d_price_block">
        <div class='d_price text-left'>
            <br>
            <span class="h4"><?=GetMessage("BLOCK_TITLE");?></span>
            <?foreach($arResult as $k=>$v){?>
                <div class="item_block">
                    <div class="row line1">
                        <div class="img col-xs-4"><img alt="<?=$v['name'],$k?>" class="img" height="66" width="66" src="<?=$v['logo_path']?>"/></div>
                        <div class="name col-xs-8"><?=$v['name']?></div>
                    </div>
                    <div class="row line">
                        <div class="description col-xs-12"><?=$v['description']?></div>
                        <?if($k == 95 || $k == 96){?>
                            <div class="price_value col-xs-12"><?=GetMessage("PRICE_TIME");?><span><?=$v['price']['MAIN']['PERIOD']?></span></div>
                        <?}?>
                        <div class="price_value col-xs-12"><?=GetMessage("PRICE_TITLE");?><span><?=$v['price']['MAIN']['PRICE']?></span> руб.</div>
                    </div>
                </div>
            <?}?>
        </div>
    </div>
    <?
}
