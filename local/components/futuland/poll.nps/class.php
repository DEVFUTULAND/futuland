<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
use Bitrix\Main\Application;

class PollNPS extends CBitrixComponent {
    function onPrepareComponentParams($arParams) {

        $this->includeModule(array("iblock"));
        $this->arResult['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
        $this->arResult['POLL_LINK'] = $arParams['POLL_LINK'];

        $arParams = array_merge(array(
            'CACHE_TIME' => 86400 * 14,
            'CACHE_TYPE' => 'Y',
        ), $arParams);

        return $arParams;
    }

    private static function includeModule(array $arModules){
        if(!empty($arModules)) {
            foreach ($arModules as $module) {
                if(!Loader::includeModule($module))
                    throw new LoaderException("Ошибка подключения модуля ".$module);
            }
        }
    }

    private function getPollFeilds() {
        $arFields = array();

        $arOrder = array(
            "SORT" => "ASC"
        );

        $arFilter = array(
            "IBLOCK_ID" => $this->arResult['IBLOCK_ID'],
            "ACTIVE" => "Y",
        );

        $dbProps = CIBlockProperty::GetList($arOrder, $arFilter);
        while($propItem = $dbProps->Fetch()) {
            $arFields[$propItem['ID']] = $propItem;
            if($propItem['PROPERTY_TYPE'] == 'L') {
                $arFields[$propItem['ID']]['VALUES'] = $this->getEnumValues($propItem['CODE']);
            }
        }

        return $arFields;
    }

    private function getEnumValues($propCode) {
        $arFieldsList = array();

        if(strlen($propCode) > 0) {
            $arFilter = array(
                "IBLOCK_ID" => $this->arResult['IBLOCK_ID'],
                "CODE" => $propCode
            );
        }

        $dbList = CIBlockPropertyEnum::GetList(array("SORT" => "ASC"), $arFilter);
        while($itemField = $dbList->Fetch()) {
            $arFieldsList[$itemField['ID']] = $itemField;
        }

        return $arFieldsList;
    }

    private function addAnswer($arProp) {
        $el = new CIBlockElement;

        $arLoadProductArray = array(
            "IBLOCK_ID"      => $this->arResult['IBLOCK_ID'],
            "PROPERTY_VALUES"=> $arProp,
            "NAME"           => "Элемент",
            "ACTIVE"         => "Y",
        );

        return $el->Add($arLoadProductArray);
    }

    public function executeComponent(){
        global $USER;

        if(Application::getInstance()->getContext()->getRequest()->isPost() && !empty($_POST)) {
            if($this->addAnswer($_POST)){
                $_SESSION['POLL_ANSWER_HB_SENT'] = true;


                if(strlen($this->arResult['POLL_LINK']) > 0)
                    LocalRedirect($this->arResult['POLL_LINK']);
            }
        } else {
            $this->arResult['USER_ID'] = $USER->GetID();
            $this->arResult['LIST'] = $this->getEnumValues('ADV_LIST');
            $this->arResult['PROPS'] = $this->getPollFeilds();
        }


        $this->IncludeComponentTemplate();
    }

}