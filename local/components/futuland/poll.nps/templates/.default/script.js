$(document).ready(function () {
    toogleElementClass('.other-checkbox', '.prop-other-wrap');
    toogleElementClass('.comment-add a', '.comment-wrapper textarea');

    $(document).on('submit', '.poll-form', function () {
        $('.poll-field-error').remove();
        if(!$('.adv-list input:checked').length) {
            $('.adv-list').before('<p class="poll-field-error">Пожалуйста, отметьте как минимум один пункт</p>');
            return false;
        }
    });

    function toogleElementClass(click, toogleEl) {
        $(document).on('click', click, function() {
            $(toogleEl).toggleClass('hidden');
        });
    }
});