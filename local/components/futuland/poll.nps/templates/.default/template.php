<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>


<section><?//echo '<pre>'; print_r($arResult['PROPS']); echo '</pre>';?>
    <?if(isset($_SESSION['POLL_ANSWER_HB_SENT']) && $_SESSION['POLL_ANSWER_HB_SENT']) {
        unset($_SESSION['POLL_ANSWER_HB_SENT']);?>
        <style>
            h1#pagetitle{display:none;}
        </style>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="text-left unreg-user-wrap">
                    <p style="color:green;margin-top:15px;">Спасибо! Обещаем активно работать над качеством оказываемых услуг.</p>
                    <p><a style="border-bottom:1px dashed;" href="/">Перейти на главную страницу</a></p>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>
    <?} else if(!empty($arResult['LIST'])) {?>
        <form class="poll-form" method="post" name="poll-form">
            <input type="hidden" value="<?=$arResult['USER_ID']?>" name="USER_ID">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <div class="text-left poll-wrap">
                        <h2>Что нам нужно улучшить?</h2>
                        <ul class="adv-list">
                            <?foreach ($arResult['LIST'] as $value) {?>
                                <li>
                                    <input name="ADV_LIST[]" class="<?=($value['VALUE'] == 'Другое' ? "other-checkbox" : "")?>" id="prop_<?=$value['ID']?>" type="checkbox" value="<?=$value['ID']?>">
                                    <label for="prop_<?=$value['ID']?>"><?=$value['VALUE']?></label>
                                </li>
                            <?}?>
                        </ul>
                        <p class="prop-other-wrap hidden">
                            <input autocomplete="off" type="text" name="OTHER" class="prop-other" placeholder="Ваш вариант" value="<?=(isset($_REQUEST['OTHER']) ? $_REQUEST['OTHER'] : "")?>">
                        </p>
                        <p class="comment-add">
                            <a href="javascript:void(0);">добавить комментарий</a>
                        </p>

                    </div>
                </div>
                <div class="col-lg-4"></div>
            </div>

            <div class="row comment-wrapper">
                <div class="col-lg-4"></div>
                <div class="col-lg-4 pd10">
                    <textarea class="hidden" placeholder="Ваш комментарий" name="COMMENT"><?=(isset($_REQUEST['COMMENT']) ? $_REQUEST['COMMENT'] : "")?></textarea>
                </div>
                <div class="col-lg-4"></div>
            </div>

            <?if(!intval($arResult['USER_ID'])) {?>
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-4">
                        <div class="text-left unreg-user-wrap">
                            <h2>Представитесь?</h2>

                            <div class="form-control">
                                <label>Ваше имя</label>
                                <input autocomplete="off" type="text" class="inputtext" name="NAME" value="<?=(isset($_REQUEST['NAME']) ? $_REQUEST['NAME'] : "")?>" placeholder="Имя">
                            </div>

                            <div class="form-control">
                                <label>Ваш телефон</label>
                                <input autocomplete="off" type="text" class="inputtext" name="PHONE" value="<?=(isset($_REQUEST['PHONE']) ? $_REQUEST['PHONE'] : "")?>" placeholder="Телефон">
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
            <?}?>

            <div class="row mt20">
                <div class="col-lg-5"></div>
                <div class="col-lg-2">
                    <input type="submit" class="btn btn-default poll-btn" value="Отправить" name="NPS_SUBMIT">
                </div>
                <div class="col-lg-5"></div>
            </div>
        </form>
    <?}?>
</section>