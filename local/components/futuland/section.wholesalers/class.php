<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc, Bitrix\Main\Application;
//Тип товара. Возможные значения: "1" - "простой товар", "2" - "комплект", "3" - "товар с торговыми предложениями", "4" - "торговое предложение".

Loc::loadMessages(__FILE__);

/**
 * Class SectionW
 * use $_SESSION['WS']
 */
final class SectionW extends CBitrixComponent
{
    public $pageSize = 10;

    public function executeComponent(){
        $cache = Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache("14400", 'wsb-sections', "wsb-section"))
        {
            $this->arResult = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            $this->getElements();
            $cache->endDataCache($this->arResult);
        }

        if(!empty($_REQUEST))
        {
            $this->setFilter();
        }

        $this->arResult["NAV_RESULT"] = $this->getNav();
        unset($this->arResult['ELEMENTS']);


        $this->includeComponentTemplate();

        return $this->arResult;
    }

    protected function setFilter()
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $requestArr = $request->getQueryList()->toArray();

        $arSection = [];
        foreach($requestArr as $k=>$v)
        {
            if(is_numeric($k)) $arSection[] = $k;
            if($k == "showBy") $this->pageSize = $v;
        }
        if(!empty($arSection)){
            foreach ($this->arResult['ELEMENTS'] as $k=>$v)
            {
                if(!in_array($v['IBLOCK_SECTION_ID'],$arSection)){
                    unset($this->arResult['ELEMENTS'][$k]);
                }
            }
        }

        return;
    }

    protected function getElements()
    {
        $this->getProducts();
        $this->getOffers();
        //bug($)
    }

    protected function getProducts()
    {
        $arSection = $this->getSectionList();

        $iterator = \CIBlockElement::GetList(
            array('name' => 'asc'),
            array('IBLOCK_ID' => C_IB_PRODUCTS,'=TYPE' => [1,3]),
            false,
            false,
            array('ID', 'NAME', 'IBLOCK_ID','TYPE','DETAIL_PAGE_URL','IBLOCK_SECTION_ID','WEIGHT', 'PRICE_'.C_PRICE_OPT,'PROPERTY_CML2_ARTICLE')
        );

        while($ar = $iterator->GetNext()) {
            if($ar['TYPE'] == '1' && empty($ar['PRICE_'.C_PRICE_OPT])) continue;

            if(!in_array($ar['IBLOCK_SECTION_ID'],$arSection)){
                $nav = CIBlockSection::GetNavChain(C_IB_PRODUCTS,$ar['IBLOCK_SECTION_ID'])->arResult;
                $ar['IBLOCK_SECTION_ID'] = $nav[1]['ID'];
            }

            if($ar['TYPE'] == '3'){
                $this->arResult["PARENTS"][$ar['ID']] = $ar;
            }else $this->arResult["ELEMENTS"][$ar['ID']] = $ar;
        }
        return;
    }

    protected function getOffers()
    {
        $iterator = \CIBlockElement::GetList(
            array('id' => 'asc'),
            array('IBLOCK_ID' => C_IB_OFFERS,'=PRICE_TYPE' => C_PRICE_OPT,'>PRICE' => 0),
            false,
            false,
            array('ID', 'NAME', 'IBLOCK_ID','TYPE','DETAIL_PAGE_URL','WEIGHT', 'PRICE_'.C_PRICE_OPT,'PROPERTY_ARTIKUL')
        );

        while($ar = $iterator->GetNext()) {
            if(strripos($ar['NAME'],"№00НФ") || strripos($ar['NAME'],"№НФНФ")) continue;
            $mxResult = CCatalogSku::GetProductInfo($ar['ID'],C_IB_OFFERS);
            $ar['PROPERTY_CML2_ARTICLE_VALUE'] = $ar['PROPERTY_ARTIKUL_VALUE'];
            $ar['IBLOCK_SECTION_ID'] = $this->arResult["PARENTS"][$mxResult['ID']]['IBLOCK_SECTION_ID'];
            $ar['PARENT'] = $mxResult['ID'];
            $this->arResult["ELEMENTS"][$ar['ID']] = $ar;
        }
        unset($this->arResult['PARENTS']);
        return;
    }

    protected function getNav()
    {
        $rs_ObjectList = new CDBResult;
        $rs_ObjectList->InitFromArray($this->arResult["ELEMENTS"]);
        $rs_ObjectList->NavStart($this->pageSize, false);
        $this->arResult["NAV_STRING"] = $rs_ObjectList->GetPageNavString("", '');
        $this->arResult["PAGE_START"] = $rs_ObjectList->SelectedRowsCount() - ($rs_ObjectList->NavPageNomer - 1) * $rs_ObjectList->NavPageSize;
        while($ar_Field = $rs_ObjectList->Fetch())
        {
            $this->arResult['ITEMS'][] = $ar_Field;
        }

        return $rs_ObjectList;
    }

    /**Make Sections array with '<=DEPTH_LEVEL'=>2 for filtering
     * @return array
     */
    protected function getSectionList()
    {
        $result = [];
        $SectList = CIBlockSection::GetList(array(), array("IBLOCK_ID"=>C_IB_PRODUCTS,"ACTIVE"=>"Y",'<=DEPTH_LEVEL'=>2) ,false, array("ID"));
        while ($SectListGet = $SectList->fetch())
        {
            $result[]=$SectListGet['ID'];
        }

        return $result;
    }


}