$(function() {
    let curProdId = '';
    // modal component
    BX.Vue.component('modal', {
        template: '#modal-template',
        data: function () {
            return {
                info: 'Загрузка...',
            }
        },
        created(){
            axios
                .get('/local/templates/futuland/components/bitrix/catalog/opt2/element.php', {params: {ELEMENT_ID: curProdId}})
                .then(response => (this.info = response.data))
                .then(function (response) {
                    console.log(curProdId);
                    // renew lazy
                    BX.Vue.create({
                        el:'.lazy_review'
                    });
                    // renew slider
                    BX.Vue.create({
                        el: '#appSlider',
                        components: {
                            agile: VueAgile,
                        }
                    })
                    //console.log(response);
                })
                .catch(error => {
                    console.log(error)
                    this.errored = true
                })
                .finally(() => this.loading = false)
        }
    });
    // modal
    let elements = document.getElementsByClassName('appVueAxios')
    for(let el of elements){
        //console.log(el)
        BX.Vue.create({
            el: el,
            data: {
                showModal: false
            },
            methods : {
                click_page   : function ( e ) {
                    curProdId = e;
                    this.showModal = true;
                    console.log(curProdId)
                },
                outside: function(e) {
                    this.clickOutside += 1;
                    //this.showModal: false;
                    //console.log(el);
                    //el.lastChild.hide();
                    this.showModal = false;
                    //console.log('clicked outside!')
                }
            },
            directives: {
                'click-outside': {
                    bind: function(el, binding, vNode) {
                        // Provided expression must evaluate to a function.
                        if (typeof binding.value !== 'function') {
                            const compName = vNode.context.name
                            let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                            if (compName) { warn += `Found in component '${compName}'` }

                            console.warn(warn)
                        }
                        // Define Handler and cache it on the element
                        const bubble = binding.modifiers.bubble
                        const handler = (e) => {
                            if (bubble || (!el.contains(e.target) && el !== e.target)) {
                                binding.value(e)
                            }
                        }
                        el.__vueClickOutside__ = handler

                        // add Event Listeners
                        document.addEventListener('click', handler)
                    },

                    unbind: function(el, binding) {
                        // Remove Event Listeners
                        document.removeEventListener('click', el.__vueClickOutside__)
                        el.__vueClickOutside__ = null

                    }
                }
            }
        });
    }

    // watch selected count
    var tableBody = document.getElementById("tableBody").rows;
    function setItemOfBid() {
        [].forEach.call(tableBody,function(el){
            let minusButton = document.getElementById('minus'+el.id);
            let plusButton = document.getElementById('plus'+el.id);
            let inputField = document.getElementById('input'+el.id);
            let fieldWeight = document.getElementById('weight'+el.id);
            let fieldPrice = document.getElementById('price'+el.id);

            let arVal = getLocalStorage();
            if(typeof(arVal) != 'undefined' && arVal != null){
                let curElVal = arVal.filter(p => p.id == el.id);
                if(curElVal[0]){
                    inputField.value = curElVal[0].count;
                    setSelectedRow(el,true);
                }
            }

            minusButton.addEventListener('click', event => {
                event.preventDefault();
                let currentValue = Number(inputField.value);
                if(currentValue <=10) {
                    inputField.value = 0;
                    setSelectedRow(el,false);
                }else{
                    inputField.value = currentValue - 10;
                    setSelectedRow(el,true);
                }
                updBid(el.id,inputField.value,fieldPrice.innerHTML,fieldWeight.innerHTML);
            });

            plusButton.addEventListener('click', event => {
                event.preventDefault();
                let currentValue = Number(inputField.value) || 0;
                inputField.value = currentValue + 10;
                setSelectedRow(el,true);
                updBid(el.id,inputField.value,fieldPrice.innerHTML,fieldWeight.innerHTML);
            });

            let timerId;
            inputField.onfocus = function() {
                var lastValue = inputField.value;
                timerId = setInterval(function() {
                    if (inputField.value != lastValue) {
                        lastValue = inputField.value;
                    }
                }, 20);
            };

            inputField.onblur = function() {
                updBid(el.id,inputField.value,fieldPrice.innerHTML,fieldWeight.innerHTML);
                if(inputField.value > 0) {
                    setSelectedRow(el,true);
                } else setSelectedRow(el,false);
                clearInterval(timerId);
            };
        });
        function setSelectedRow(el,bool) {
            if (bool === true){
                el.classList.add("selected");
            }else el.classList.remove("selected");
        }
    }
    setItemOfBid();

    // final block - button create & string maked id_bid
    var vfinal = BX.Vue.create({
        el: '#createBid',
        data: {
            id_bid: false,
            show_button: false
        },
        methods : {
            submit(){
                axios
                    .get('/wsb/bids.php', {params: {type: "setBid",arr: localStorage.getItem("bid")}})
                    .then(response => (this.info = response.data))
                    .then(function (response) {
                        if(response.error === "N"){
                            //console.log(response);
                            vfinal.updateValue(response.desc);
                            vfinal.show_button = false;
                            localStorage.removeItem("bid");
                            vfinal.clearTable()
                        }
                        if(response.error === "Y"){
                            console.log("error === Y");
                        }
                    })
                    .catch(error => {
                        console.log(error)
                        this.errored = true
                    })
                    .finally(() => this.loading = false)
            },
            updateValue (value) {
                vfinal.id_bid = "Заявка "+value+" создана";
            },
            updateShowButton(value){
                vfinal.show_button = value;
            },
            clearTable(){
                let arrReset = document.querySelectorAll("input[id^='input']");
                arrReset.forEach(function(item, i, arrReset) {item.value = 0});

                let trReset = document.getElementById("tableBody").rows;
                for(let item of trReset) item.classList.remove("selected");
            }
        }
    });

    // summary block - count,weight & summ
    function updSummary() {
        let pars = getLocalStorage();
        countSummary.innerHTML = pars.length;

        let allWeight = [];
        let allSumm = [];

        Object.entries(pars).forEach(([key, val]) => {
            allSumm.push(+val.price * +val.count);
            allWeight.push(+val.weight * +val.count);
        });

        summSummary.innerHTML = allSumm.reduce(function(allSumm, num){ return allSumm + num }, 0);
        weightSummary.innerHTML = allWeight.reduce(function(allWeight, num){ return allWeight + num }, 0) / 1000;

        if(pars.length > 0){
            vfinal.updateShowButton(true);
        }else{
            vfinal.updateShowButton(false);
        }
    }

    // add el to bid
    function updBid(id,count,price,weight) {
        let oldElements = getLocalStorage();
        let newElements = oldElements.filter(p => p.id !== id);
        localStorage.removeItem("bid");
        if(count > 0){
            let elem = {"id":id,"count":count,"price":price,"weight":weight};
            newElements.push(elem);
        }
        localStorage.setItem("bid", JSON.stringify(newElements));

        updSummary();
    }
    updBid();

    function getLocalStorage() {
        return JSON.parse(localStorage.getItem("bid") || "[]");
    }
});