<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset,Bitrix\Main\UI\Extension;
Asset::getInstance()->addJs( "/local/lib/npm/node_modules/axios/dist/axios.min.js");
Asset::getInstance()->addJs( "/local/lib/npm/node_modules/vue-agile/dist/VueAgile.umd.min.js");


Extension::load("ui.vue");
Extension::load("ui.vue.directives.lazyload");
//bug($arResult["NAV_RESULT"]);
//bug($arResult["NAV_RESULT"]->NavPageSize);
//bug(array_pop($arResult['ITEMS']));
//bug(array_rand ($arResult['ELEMENTS'],100));
//$rand = array_rand ($arResult['ELEMENTS'],100);
?>
<style>
    #ws-section .module-pagination{padding:unset}
    #ws-section .maininfo span{font-weight: bold}
    #ws-section .maininfo .number_list a.current{color:var(--bl)}
    #ws-section #summary{margin-bottom: 10px;}
    #ws-section table tr td a{color:var(--bl)}
    #ws-section table tr.selected{background-color:#fcf8e5}
    #ws-section table tr td a:hover{color:var(--primary2)}
    #ws-section .appVueAxios{position: relative}
    #ws-section table .btn.btn-primary{padding: 2px 8px;}
    .modal-mask {
        position: absolute;
        z-index: 9998;
        top: 4rem;
        left: 0;
        width: 800%;
        min-width: 700px;
        height: auto;
        background-color: rgba(0, 0, 0, .5);
        display: table;
        transition: opacity .3s ease;
    }
    .modal-wrapper {display: table-cell;vertical-align: middle;}
    .modal-container {
        width: 100%;
        height:70%;
        max-height: 500px;
        margin: 0;
        padding: 1rem;
        background-color: #fff;
        border-radius: 2px;
        box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
        transition: all .3s ease;
        overflow: auto;
    }
    .modal-header h3 {margin-top: 0;color: #42b983;}
    .modal-body { margin: 20px 0;}
    .modal-default-button {float: right;border: none;background-color: unset;}
    .modal-enter,.modal-leave-active {opacity: 0;}
    .modal-enter .modal-container,.modal-leave-active .modal-container {-webkit-transform: scale(1.1);transform: scale(1.1);}
    .modal-mask #appSlider *{max-height:490px;}
    .modal-mask #appSlider .agile__actions{display:none !important}
    #ws-section tr td input[type="number"]{/*text-align: center;*/width:60px;}
    #ws-section tr td button[id^="minus"],#ws-section tr td button[id^="plus"]{height: 24px;width: 24px}
    #existedBid{color:red;font-weight: bold;}
    [v-cloak] {
        display: none;
    }
</style>
<hr>
<div id="ws-section">
    <div class="row maininfo">
        <div class="col-4">
            <span class="show_title">Всего:&nbsp<?=$arResult["NAV_RESULT"]->NavRecordCount?></span>
        </div>
        <div class="col-4">
            <?$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array('NAV_RESULT' => $arResult["NAV_RESULT"]));?>
        </div>
        <div class="col-4 text-right">
            <span class="show_title">Показать по:&nbsp&nbsp</span>
            <span class="number_list">
                <?$pec = [10,50,100];
                foreach($pec as $v){?>
                     <a rel="nofollow" <?=($v == $arResult["NAV_RESULT"]->NavPageSize)? "class='current'" : '' ?>
                     href="<?= $APPLICATION->GetCurPageParam('showBy='.$v, array('showBy', 'mode')) ?>">&nbsp<?=$v?></a>
                <?}?>
            </span>
        </div>
    </div>
    <hr>
    <div id="summary" class="row maininfo">
        <div class="col-3"><span>Выбрано: <span id="countSummary">0</span></span></div>
        <div class="col-3 text-center"><span>Общий вес(кг): <span id="weightSummary">0</span></span></div>
        <div class="col-3 text-center"><span>Общая стоимость(руб): <span id="summSummary">0</span></span></div>
        <div class="col-3 text-right">
            <div id="createBid" v-cloak>
                <div v-if="id_bid" id="existedBid">{{ id_bid }}</div>
                <button v-if="show_button" @click="submit" class="btn btn-primary">Создать заявку</button>
            </div>
        </div>
    </div>

    <table class="table table-hover">
        <thead class="thead-light">

            <tr>
                <th scope="col">ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Артикул</th>
                <th scope="col">Вес, гр.</th>
                <th scope="col">Оптовая цена, руб.</th>
                <th scope="col">Количество</th>
            </tr>
        </thead>
        <tbody id="tableBody">
            <?foreach($arResult['ITEMS'] as $k=>$v)
                {?>
                    <tr id="<?=$v['ID']?>">
                        <td>
                            <div class="appVueAxios" v-click-outside="outside">
                                <button id="show-modal" class="btn btn-primary" @click="click_page('<?=($v["TYPE"] == '4')? $v['PARENT'] : $v['ID']?>')">Подробнее...</button>
                                <modal v-if="showModal" @close="showModal = false" ></modal>
                            </div>
                        </td>
                        <td><a target="_blank" href="<?=$v['DETAIL_PAGE_URL']?>"><?=$v['NAME']?></a></td>
                        <td><?=$v['PROPERTY_CML2_ARTICLE_VALUE']?></td>
                        <td id="weight<?=$v['ID']?>"><?=$v['WEIGHT']?></td>
                        <td id="price<?=$v['ID']?>"><?=(int)$v['PRICE_11']?></td>
                        <td>
                            <div id="counter_block<?=$v['ID']?>">
                                <button id="minus<?=$v['ID']?>">−</button>
                                <input type="number" value="0" id="input<?=$v['ID']?>" size="4" min="0" max="10000" oninput="validity.valid||(value='');"/>
                                <button id="plus<?=$v['ID']?>">+</button>
                            </div>
                        </td>
                    </tr>
               <?}?>
        </tbody>
    </table>
    <p><?$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array('NAV_RESULT' => $arResult["NAV_RESULT"]));?></p>
</div>

<!-- template for the modal component -->
<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="$emit('close')">
                <div class="modal-container" @click.stop="">
                    <button class="modal-default-button" @click="$emit('close')"><i class="fa fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div name="body" v-html="info">
                            {{ info }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>

