<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script type="text/javascript">
    $(function() {
        $('.popup.geoloc').fancybox({
            'wrapCSS' : 'popup_select_loc',
            'overlayShow': false, // значения параметров можно посмотреть на сайте разработчика
            'padding': 0,
            'margin' : 0,
            'scrolling' : 'no',
            'titleShow': false,
            'type': 'ajax',
            'href': '/ajax/form.php?form_id=geoloc'
        });
    });
</script>

<div id='locGeoIp' class='f_region text-center'>
    <div class='city_title col-xs-12'><i class="fa fa-map-marker"></i><?=GetMessage("CITY_TITLE");?><i class="fa fa-angle-down"></i></div>
    <div class='popup geoloc col-xs-12' data-event='jqm' data-name='' data-param-form_id='geoloc'>
        <span class='cityName'><?=$arResult['CITY_NAME']?></span>
    </div>
</div>
