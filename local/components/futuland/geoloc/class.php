<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Service\GeoIp;
//use Bitrix\Main\SystemException;
use Bitrix\Sale\Location\GeoIp as SaleGeoip;

Loc::loadMessages(__FILE__);

final class GeoLoc extends CBitrixComponent
{
    public function executeComponent(){//$_SESSION['FUTU']['REGION']
        /* 1. проверяем, сохранено ли местоположение в LOCATION.
         * 2. если нет - определяем и устанавливаем по GEO_IP.
         * 3. else - ставим DEFAULT */
        $is_bot = preg_match(
            "~(Google|Yahoo|Rambler|Bot|Yandex|Spider|Snoopy|Crawler|Finder|Mail|curl)~i",
            $_SERVER['HTTP_USER_AGENT']
        );
        if($is_bot){
            $this->setLocDefault();
        } elseif(empty($_SESSION['FUTU']['REGION']['METHOD'])){
            $this->setLocFromGeoIP();
        } elseif ($_SESSION['FUTU']['REGION']['METHOD'] == "manual"){
            $this->arResult['CITY_NAME'] = $_SESSION['FUTU']['REGION']['LOCATION']['DISPLAY'];
        } elseif ($_SESSION['FUTU']['REGION']['METHOD'] == "geo_ip"){
            $this->arResult['CITY_NAME'] = $_SESSION['FUTU']['REGION']['GEOIP']['cityName'];
        } else $this->setLocDefault();

        $this->includeComponentTemplate();

        return $this->arResult;
    }

    function setLocFromGeoIP(){
        $ip = $_SERVER["REMOTE_ADDR"];
        //$ip = '171.25.248.1';//Переславль-Залесский
        /*
        $ip = '109.248.218.1';//Подольск
        $ip = '171.25.248.1';//Переславль-Залесский
        $ip = '93.178.204.228';//киев
        */
        $geoResult = GeoIp\Manager::getDataResult($ip, 'ru');

        if($geoResult){
            if ($geoResult->isSuccess()) {
                $obGeoData = $geoResult->getGeoData();
                $arr = (array) $obGeoData;

                //var_dump($arr);
                if(is_null($arr["cityName"]) || is_null($arr["countryCode"]) || $arr["countryCode"] != "RU"){
                    $this->setLocDefault();
                    return;
                }else{
                    $_SESSION['FUTU']['REGION']['GEOIP'] = $arr;
                    $_SESSION['FUTU']['REGION']['METHOD'] = "geo_ip";
                    $_SESSION['FUTU']['REGION']['BX_LOC_CODE'] = SaleGeoip::getLocationCode($ip, 'ru');
                    $this->arResult['CITY_NAME'] = $arr["cityName"];
                }
            }
        }
    }

    function setLocDefault(){
        $city = GetMessage('CITY_DEFAULT');
        $_SESSION['FUTU']['REGION']['METHOD'] = "default";
        $_SESSION['FUTU']['REGION']['BX_LOC_CODE'] = GetMessage('CODE_DEFAULT');
        $this->arResult['CITY_NAME'] = $city;
    }
}