<?php
/**
 * Created by PhpStorm.
 * User: Jager
 * Date: 22.07.19
 * Time: 13:56
 */

namespace FUTU;

use Bitrix\Main\Loader;
use CIBlockElement;
use CIBlockPropertyEnum;
use CUser;

Loader::includeModule('iblock');

/**
 * Class Fwholesalers
 * @package FUTU
 * used define("C_IB_WS", 223);
 */
class Fwholesalers
{
    /** status runtime export
     * @var bool
     */
    protected static $runtime_export_bids = false;
    protected static $runtimeArBids = null;

    // MAIN METHOD - создание заявок для 1с
    static function makeXml()
    {
        try {
            $arBids = self::getBidsStatusN();

            if($arBids){
                self::$runtime_export_bids = true;
                self::$runtimeArBids = array_keys($arBids);

                $allBids = (new Fwholesalers)->prepareXml($arBids);

                self::setBidsStatusY(self::$runtimeArBids);

                return $allBids;
            } else return 'empty';

        } catch (\Exception $e) {
            dfile(date('c').PHP_EOL.$e->getMessage(),'makeXml.log','/wsb/');
        } finally {
            dfile(date('c').PHP_EOL.serialize(self::$runtimeArBids),'makeXml.log','/wsb/');
            self::$runtime_export_bids = false;
            self::$runtimeArBids = null;
        }
    }

    // получить все невыгруженные заявки
    static function getBidsStatusN()
    {
        $result = self::getBids(['!PROPERTY_STATUS_VALUE' => "Y"]);
        if(is_null($result)){
            return false;
        }else return $result;
    }

    // пометить выгруженные в 1с заказы
    static function setBidsStatusY(array $bids)
    {
        foreach ($bids as $k=>$v)
        {
            self::updateBid($v,'Y');
        }
        return;
    }

    /** создание заявки
     * @param $userId
     * @param $arElem = [["VALUE"=>"38704","DESCRIPTION"=>"111"]]
     *      VALUE = id Goods, DESCRIPTION = count Goods
     *
     */
    static function setBid($arElem)
    {

        $userId = CUser::GetID();
        $arItems = self::prepareArr($arElem);
        $propStatusList = self::propStatusList();
        $arFields = [
            "ACTIVE" => "Y",
            "IBLOCK_ID" => C_IB_WS,
            "NAME" => "WSB-".$userId."-".date("d.m.Y h:i"),
            "PROPERTY_VALUES" => [
                'USER' => $userId,
                'STATUS' => $propStatusList['N'],
                'GOODS' => $arItems
            ]
        ];
        $bid = new CIBlockElement();
        $idBid = $bid->Add($arFields, false, false, false);

        $result = ["error" => '',"desc" => ''];
        if($idBid){
            $result["error"] = "N";$result["desc"] = $idBid;
        } else {
            dfile(date('c').PHP_EOL.$bid->LAST_ERROR,'setBid.log','/wsb/');
            $result["error"] = 'Y';$result["desc"] = $bid->LAST_ERROR;
        }
        return $result;
    }

    protected function prepareArr($arr)
    {
        $result = [];
        foreach ($arr as $k=>$v)
        {
            $result[] = ["VALUE" => $v['id'],"DESCRIPTION" => $v['count']];
        }
        return $result;
    }

    static function updateBid($Id,$status)
    {
        $propStatusList = self::propStatusList();

        $bid = new CIBlockElement();
        $bid::SetPropertyValuesEx($Id, C_IB_WS, ['STATUS' => $propStatusList[$status]]);

        return;
    }

    // получить все заявки конкретного оптовика - для личного кабинета
    static function getUserBids($userid)
    {
        $result = self::getBids(['PROPERTY_USER' => $userid]);
        if(!empty($result)){
            krsort($result);
            //$result = self::getUserBidsProducts($result);
        }

        return $result;
    }

    static function getUserBidsProducts($arr)
    {
        $iterator = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => [C_IB_PRODUCTS,C_IB_OFFERS],"=ID" => $arr['VALUES']),
            false,
            false,
            array("ID",'NAME','DETAIL_PAGE_URL')
        );
        $arProducts = [];
        while ($ar = $iterator->GetNext())
        {
            $arProducts[$ar["ID"]] = ['NAME' => $ar['NAME'],'DETAIL_PAGE_URL' => $ar['DETAIL_PAGE_URL']];
        }

        $result = [];
        foreach($arr['VALUES'] as $k=>$v)
        {
            $result[$v] = $arProducts[$v];
            $result[$v]['COUNT'] = $arr['COUNT'][$k];
        }

        return $result;
    }

    // получить заявки используя фильтр
    function getBids($filter=null)
    {
        $result = null;
        $iblockID = ['IBLOCK_ID' => C_IB_WS];
        $arFilter = array_merge($iblockID,$filter);

        $iterator = \CIBlockElement::GetList(
            array(),
            array($arFilter),
            false,
            false,
            array("ID",'IBLOCK_ID','DATE_CREATE',"PROPERTY_USER","PROPERTY_STATUS")
        );

        while ($ar = $iterator->GetNextElement()) {
            $arFields = $ar->GetFields();
            $arGoods = $ar->GetProperty('GOODS');
            $arRes['VALUES'] = $arGoods['VALUE'];
            $arRes['COUNT'] = $arGoods['DESCRIPTION'];

            $arFields['GOODS'] = $arRes;
            $result[$arFields['ID']] = $arFields;
        }
        return $result;
    }

    protected function getUserInfo($id)
    {
        $rsUser = CUser::GetByID($id);
        $arUser = $rsUser->Fetch();

        return $arUser;
    }

    protected function getGoodsInfo($arItems)
    {
        $result = [];
        $arr_combine = array_combine($arItems['VALUES'],$arItems['COUNT']);//convert $arItems for comfort

        $iterator = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => [C_IB_PRODUCTS,C_IB_OFFERS],'ID' => $arItems['VALUES']),
            false,
            false,
            array('ID','IBLOCK_ID','NAME','XML_ID','IBLOCK_EXTERNAL_ID',"PRICE_".C_PRICE_OPT,"PRICE_".C_PRICE_RETAIL)
        );

        while($ar = $iterator->fetch()) {
            $result[$ar['ID']] = $ar;
            $result[$ar['ID']]['COUNT'] = $arr_combine[$ar['ID']];
        }

        return $result;
    }

    protected function prepareXml($arBids)
    {
        $dom = new \DOMDocument('1.0','utf-8');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;

        $root = $dom->createElement("КоммерческаяИнформация");
        $root->setAttribute("ВерсияСхемы","3.01");
        $root->setAttribute("ДатаФормирования",date('Y-m-d'));
        $dom->appendChild($root);

        foreach($arBids as $k=>$v)
        {
            // prepare data
            $arUser = $this->getUserInfo($v['PROPERTY_USER_VALUE']);
            $arGoods = $this->getGoodsInfo($v['GOODS']);
            $userPhone = !empty($arUser['UF_YURPHONE']) ? $arUser['UF_YURPHONE'] : (!empty($arUser['PERSONAL_PHONE']) ? $arUser['PERSONAL_PHONE'] : (!empty($arUser['WORK_PHONE']) ? $arUser['WORK_PHONE'] : '' ));
            $allSum = 0;
            foreach($arGoods as $k1=>$v1){
                $price = $v1["PRICE_".C_PRICE_OPT] ? $v1["PRICE_".C_PRICE_OPT] : $v1["PRICE_".C_PRICE_RETAIL];
                $allSum += (float)$v1['COUNT'] * (float)$price;
            }
            $allSum = number_format((float)$allSum, 4, '.', '');


            $container = $dom->createElement("Контейнер");
            $root->appendChild($container);

            $document = $dom->createElement("Документ");
            $container->appendChild($document);

            $id = $dom->createElement("Ид", 'ОПТ-'.$k);
            $document->appendChild($id);
            $number = $dom->createElement("Номер", 'ОПТ-'.$k);
            $document->appendChild($number);
            $date = $dom->createElement("Дата", ConvertDateTime($v['DATE_CREATE'], "YYYY-MM-DD", "ru"));
            $document->appendChild($date);
            $operation = $dom->createElement("ХозОперация", 'Заказ товара');
            $document->appendChild($operation);
            $role = $dom->createElement("Роль", 'Продавец');
            $document->appendChild($role);
            $currency = $dom->createElement("Валюта", 'руб');
            $document->appendChild($currency);
            $rate = $dom->createElement("Курс", 1);
            $document->appendChild($rate);
            $total = $dom->createElement("Сумма", $allSum);
            $document->appendChild($total);
            $time = $dom->createElement("Время", ConvertDateTime($v['DATE_CREATE'], "HH:MI:SS", "ru"));
            $document->appendChild($time);

            // Информация об оптовике
            $users = $dom->createElement("Контрагенты");
            $document->appendChild($users);
            $user = $dom->createElement("Контрагент");
            $users->appendChild($user);
            $userID = $dom->createElement("ИД", $arUser['ID']);
            $user->appendChild($userID);
            $userName = $dom->createElement("Наименование", $arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME']);
            $user->appendChild($userName);
            $userRole = $dom->createElement("Роль", 'Покупатель');
            $user->appendChild($userRole);
            $userInn = $dom->createElement("ИНН", $arUser['UF_INN']);
            $user->appendChild($userInn);

            $userContacts = $dom->createElement("Контакты");
            $user->appendChild($userContacts);

            $userContact1 = $dom->createElement("Контакт");
            $userContacts->appendChild($userContact1);
            $userPhoneType = $dom->createElement("Тип", 'ТелефонРабочий');
            $userPhoneVal = $dom->createElement("Значение", $userPhone);
            $userContact1->appendChild($userPhoneType);
            $userContact1->appendChild($userPhoneVal);

            $userContact2 = $dom->createElement("Контакт");
            $userContacts->appendChild($userContact2);
            $userMailType = $dom->createElement("Тип", 'Почта');
            $userMailVal = $dom->createElement("Значение", $arUser['EMAIL']);
            $userContact2->appendChild($userMailType);
            $userContact2->appendChild($userMailVal);

            // Товары
            $goods = $dom->createElement("Товары");
            $document->appendChild($goods);
            foreach ($arGoods as $p)
            {
                $good = $dom->createElement("Товар");
                $goods->appendChild($good);

                $goodId = $dom->createElement("Ид",$p['XML_ID']);
                $good->appendChild($goodId);
                $goodCId = $dom->createElement("ИдКаталога",$p['IBLOCK_EXTERNAL_ID']);
                $good->appendChild($goodCId);
                $goodName = $dom->createElement("Наименование",$p['NAME']);
                $good->appendChild($goodName);
                $goodMeasure = $dom->createElement("БазоваяЕдиница",'шт');
                $goodMeasure->setAttribute("Код","796");
                $goodMeasure->setAttribute("НаименованиеПолное",'Штука');
                $goodMeasure->setAttribute("МеждународноеСокращение",'PCE');
                $good->appendChild($goodMeasure);

                $goodPrice = $dom->createElement("ЦенаЗаЕдиницу",number_format((float)$p["PRICE_".C_PRICE_OPT] ? $p["PRICE_".C_PRICE_OPT] : $p["PRICE_".C_PRICE_RETAIL], 4, '.', ''));
                $good->appendChild($goodPrice);
                $goodCount = $dom->createElement("Количество",$p['COUNT']);
                $good->appendChild($goodCount);
                $goodSum = $dom->createElement("Сумма",$p['COUNT'] * ($p["PRICE_".C_PRICE_OPT] ? $p["PRICE_".C_PRICE_OPT] : $p["PRICE_".C_PRICE_RETAIL]));
                $good->appendChild($goodSum);

                // Значения реквизитов
                $propsGood = $dom->createElement("ЗначенияРеквизитов");
                $good->appendChild($propsGood);
                $arGoodProps = ["ВидНоменклатуры"=>"Товар","ТипНоменклатуры"=>"Товар",
                    "СвойствоКорзины#CATALOG.XML_ID" => $p['IBLOCK_EXTERNAL_ID'],"СвойствоКорзины#PRODUCT.XML_ID" => $p['XML_ID']];

                foreach ($arGoodProps as $k=>$v)
                {
                    $prop = $dom->createElement("ЗначениеРеквизита");
                    $propName = $dom->createElement("Наименование",$k);
                    $propValue = $dom->createElement("Значение",$v);
                    $prop->appendChild($propName);
                    $prop->appendChild($propValue);
                    $propsGood->appendChild($prop);
                }
            }

            // Значения реквизитов
            $props1c = $dom->createElement("ЗначенияРеквизитов");
            $document->appendChild($props1c);

            $propSite = $dom->createElement("ЗначениеРеквизита");
            $propSiteName = $dom->createElement("Наименование","Сайт");
            $propSiteValue = $dom->createElement("Значение","[s0] futuopt.ru");
            $propSite->appendChild($propSiteName);
            $propSite->appendChild($propSiteValue);
            $props1c->appendChild($propSite);

            $propType = $dom->createElement("ЗначениеРеквизита");
            $propTypeName = $dom->createElement("Наименование","Вид заказа");
            $propTypeValue = $dom->createElement("Значение","Оптовый");
            $propType->appendChild($propTypeName);
            $propType->appendChild($propTypeValue);
            $props1c->appendChild($propType);
        }

        $xml_result = $dom->saveXML();
        return $xml_result;
    }

    /** получение полей свойства Статус
     * @return array
     */
    protected static function propStatusList()
    {
        $result = [];
        $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>C_IB_WS));
        while($enum_fields = $property_enums->GetNext())
        {
            $result[$enum_fields['VALUE']] = $enum_fields['ID'];
        }
        return $result;
    }
}