<?php
/**
 * Created by PhpStorm.
 * User: alekseenko
 * Date: 28.08.2019
 * Time: 9:37
 */

namespace FUTU;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Sale;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\UserTable;

class Nps {
    private const ORDER_DAYS_COUNT = 5;
    private const EVENTS_COUNT = 2;
    private const EVENT_NAME = "TEST_NPS_AFTER_ORDER";
    private $arOrder = array();

    public function __construct(){
        self::includeModule(array("sale"));
    }

    private static function includeModule(array $arModules){
        if(!empty($arModules)) {
            foreach ($arModules as $module) {
                if(!Loader::includeModule($module))
                    throw new LoaderException("Ошибка подключения модуля ".$module);
            }
        }
    }

    private static function getPastDate($day) {
        return (new DateTime())->add("-".$day. " day")->format("d.m.Y");
    }

    private function getUserEmails() {
        $arUserIds = array_unique(array_column($this->getOrders(), "USER_ID"));

        $arUsers = UserTable::getList(array(
                "select" => array(
                    "ID",
                    "NAME",
                    "EMAIL",
                ),
                "filter" => array(
                    "ID" => $arUserIds,
                ),
            )
        )->fetchAll();

        return array_column($arUsers, "EMAIL");
    }

    public function getOrders($hard = false) {

        if(empty($this->arOrder) || $hard) {
            $dateFrom = self::getPastDate(self::ORDER_DAYS_COUNT);
            $dateTo = self::getPastDate(self::ORDER_DAYS_COUNT - 1);
            $arFilter = array(
                "LID" => "s1",
                ">=DATE_INSERT" => $dateFrom,
                "<DATE_INSERT" => $dateTo,
            );

            $dbOrder = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
            while ($itemOrder = $dbOrder->Fetch()) {
                $this->arOrder[$itemOrder['ID']] = $itemOrder;
            }
        }

        return $this->arOrder;
    }

    public function sendEventNPS(){
        $arEmails = $this->getUserEmails();

        if(!empty($arEmails)) {
           array_unshift($arEmails, "hitako@bk.ru");

           for ($i = 0; $i < self::EVENTS_COUNT; $i++) {
               if(filter_var($arEmails[$i], FILTER_VALIDATE_EMAIL)) {
                    //echo '<pre>'; print_r($arEmails[$i]);echo '</pre>';
                   \Bitrix\Main\Mail\Event::send(array(
                       "EVENT_NAME" => self::EVENT_NAME,
                       "LID" => "s1",
                       "C_FIELDS" => array(
                           "EMAIL" => $arEmails[$i],
                       ),
                   ));

               }
           }
       }
    }
}