<?php
namespace FUTU;

\Bitrix\Main\Loader::includeModule('highloadblock');
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;


class Fhl
{

    function getEntityDataClass($hlid) {
        if (empty($hlid) || $hlid < 1)
            return false;
        $hlblock = HL\HighloadBlockTable::getById($hlid)->fetch();
        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        return $entity_data_class;
    }

    static function getOne($hlid,$id){
        $entity_data_class = self::getEntityDataClass($hlid);
        $res= $entity_data_class::getList(array(
            'select' => array('*'),
            'filter' => ['UF_XML_ID' => $id]
        ));

        $row = $res->fetch();
        return $row;
    }

    static function getList($hlid){
        $entity_data_class = self::getEntityDataClass($hlid);
        $res = $entity_data_class::getList(array(
            'select' => array('*')
        ));
        $result=[];
        while($el = $res->fetch()){
            $result[$el['UF_XML_ID']] = $el;
        }
        return $result;
    }
}