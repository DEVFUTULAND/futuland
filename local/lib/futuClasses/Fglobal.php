<?php
/**
 * Created by PhpStorm.
 * User: Jager
 * Date: 12.07.19
 * Time: 9:04
 * example use:  Fglobal::Lazy()['preload'];
 */

namespace FUTU;


class Fglobal
{
    private static $theme_path = "/local/templates/futuland";
    protected static $WS_CAT_PARAMS;

    public static function Lazy()
    {
        return ['preload' => static::$theme_path . "/images/loaders/preloader.gif",
            'error' => static::$theme_path . "/images/no_photo_small.png"];
    }

    static function get_product_day_id()
    {
        $result = false;

        $iterator = \CIBlockElement::GetList(
            array('cnt' => 1),
            array('IBLOCK_ID' => C_IB_PRODUCTS, "ACTIVE" => "Y", "PROPERTY_PRODUCT_DAY" => date("Y-m-d")),
            false,
            false,
            array('ID', 'NAME', 'IBLOCK_ID', 'PROPERTY_PRODUCT_DAY')
        );

        if ($el = $iterator->fetch()) {
            $result = $el['ID'];
        }

        if($result){
            return $result;
        } else return false;
    }
}