<?php

class SetLocation
{
    static $curCityId;
    static $curCityName;

    /**
     * ID свойств заказа
     */
    const PROP_LOCATION = 6;
    const PROP_ZIP = 4;
    const PROP_LOCATION_NAME = 5;


    static function OnSaleComponentOrderProperties(&$arFields)
    {
        if(!empty($_SESSION['FUTU']['REGION']['LOCATION']['VALUE']) && !empty($_SESSION['FUTU']['REGION']['LOCATION']['DISPLAY'])){
            self::$curCityId = $_SESSION['FUTU']['REGION']['LOCATION']['VALUE'];
            self::$curCityName = $_SESSION['FUTU']['REGION']['LOCATION']['DISPLAY'];


            $rsLocaction = CSaleLocation::GetLocationZIP(self::$curCityId);
            $arLocation = $rsLocaction->Fetch();
            $arFields['ORDER_PROP'][self::PROP_ZIP] = $arLocation['ZIP'];
            $arFields['ORDER_PROP'][self::PROP_LOCATION_NAME] = self::$curCityName;
            $arFields['ORDER_PROP'][self::PROP_LOCATION] = CSaleLocation::getLocationCODEbyID(self::$curCityId);
        }
        return false;
    }
}
