<?php
if(!isset($_SERVER['HTTP_HOST'])) {
    $_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__) . "/../../../..");

    define("NO_KEEP_STATISTIC", true);
    define("NOT_CHECK_PERMISSIONS", true);
    define("NEED_AUTH", false);

    require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . '/local/lib/futuClasses/Nps.php';

    $nps = new \FUTU\Nps();
    $nps->sendEventNPS();
}