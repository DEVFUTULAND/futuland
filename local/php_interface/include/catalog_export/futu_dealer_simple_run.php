<?
//<title>Futu simple</title>
/** @global CMain $APPLICATION */
/** @var array $YANDEX_EXPORT */
/** @var string $SETUP_FILE_NAME */
use Bitrix\Currency, Bitrix\Main\Loader;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Entity\ExpressionField;

Loader::includeModule("catalog");

IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/catalog/export_setup_templ.php');
set_time_limit(2400);
ini_set('memory_limit', '-1');

global $USER, $APPLICATION;

if(!isset($_SERVER['HTTP_HOST'])) {
	$USER->Authorize(1);
}

function yandex_replace_special($arg)
{
	if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
		return $arg[0];
	else
		return " ";
}

function yandex_text2xml($text, $bHSC = false, $bDblQuote = false)
{
	global $APPLICATION;

	$bHSC = (true == $bHSC ? true : false);
	$bDblQuote = (true == $bDblQuote ? true: false);

	if ($bHSC)
	{
		$text = htmlspecialcharsbx($text);
		if ($bDblQuote)
			$text = str_replace('&quot;', '"', $text);
	}
	$text = preg_replace('/[\x01-\x08\x0B-\x0C\x0E-\x1F]/', "", $text);
	$text = str_replace("'", "&apos;", $text);
	$text = $APPLICATION->ConvertCharset($text, LANG_CHARSET, 'windows-1251');
	return $text;
}

$strExportErrorMessage = '';

$usedProtocol = (isset($USE_HTTPS) && $USE_HTTPS == 'Y' ? 'https://' : 'http://');

$SETUP_SERVER_NAME = trim($SETUP_SERVER_NAME);
if (strlen($SETUP_FILE_NAME)<=0)
{
	$strExportErrorMessage .= GetMessage("CET_ERROR_NO_FILENAME")."<br>";
}
elseif (preg_match(BX_CATALOG_FILENAME_REG,$SETUP_FILE_NAME))
{
	$strExportErrorMessage .= GetMessage("CES_ERROR_BAD_EXPORT_FILENAME")."<br>";
}

if ($strExportErrorMessage == '')
{
	$SETUP_FILE_NAME = Rel2Abs("/", $SETUP_FILE_NAME);

	if ($APPLICATION->GetFileAccessPermission($SETUP_FILE_NAME) < "W")
	{
		$strExportErrorMessage .= str_replace("#FILE#", $SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_ACCESS_DENIED'))."\n";
	}
}

if ($strExportErrorMessage == '')
{
	if (!$fp = @fopen($_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, "wb"))
	{
		$strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_OPEN_WRITING'))."\n";
	}
	else
	{
		if (!@fwrite($fp, "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n"))
		{
			$strExportErrorMessage .= str_replace('#FILE#',$_SERVER["DOCUMENT_ROOT"].$SETUP_FILE_NAME, GetMessage('CET_YAND_RUN_ERR_SETUP_FILE_WRITE'))."\n";
			@fclose($fp);
		}
		else
		{
            file_put_contents($fp, '');
		    /*
                fwrite($fp, '<? $strReferer1 = htmlspecialchars($_GET["referer1"]); ?>');
                fwrite($fp, '<?if (!isset($_GET["referer2"]) || strlen($_GET["referer2"]) <= 0) $_GET["referer2"] = "";?>');
                fwrite($fp, '<? $strReferer2 = htmlspecialchars($_GET["referer2"]); ?>');
			*/
		}
	}
}

if ($strExportErrorMessage == '')
{
	/*fwrite($fp, '<? header("Content-Type: text/xml; charset=windows-1251");?>');*/
	/*fwrite($fp, "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n");*/
	fwrite($fp, "<!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">\n");
	fwrite($fp, "<yml_catalog date=\"".date("Y-m-d H:i")."\">\n");
	fwrite($fp, "<shop>\n");
	fwrite($fp, "<name>Futuland.ru</name>\n");
	fwrite($fp, "<company>".$APPLICATION->ConvertCharset("ИП Фархулин Геннадий Вячеславович", LANG_CHARSET, 'windows-1251')."</company>\n");
	fwrite($fp, "<url>".$usedProtocol.htmlspecialcharsbx(strlen($SETUP_SERVER_NAME) > 0 ? $SETUP_SERVER_NAME : COption::GetOptionString("main", "server_name", ""))."</url>\n");
	fwrite($fp, "<platform>BSM/Yandex/Market</platform>\n");
    fwrite($fp, "<version>1.2.1</version>\n");

	$BASE_CURRENCY = Currency\CurrencyManager::getBaseCurrency();
	$RUR = 'RUR';
	$currencyIterator = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY'),
		'filter' => array('=CURRENCY' => 'RUR')
	));
	if ($currency = $currencyIterator->fetch())
		$RUR = 'RUR';
	unset($currency, $currencyIterator);

	$allowedCurrency = array('RUR','RUB');
	$strTmp = "<currencies>\n";
	$currencyIterator = Currency\CurrencyTable::getList(array(
		'select' => array('CURRENCY', 'SORT'),
		'filter' => array('@CURRENCY' => $allowedCurrency),
		'order' => array('SORT' => 'ASC', 'CURRENCY' => 'ASC')
	));
	while ($currency = $currencyIterator->fetch())
		$strTmp .= '<currency id="'.$currency['CURRENCY'].'" rate="'.(CCurrencyRates::ConvertCurrency(1, $currency['CURRENCY'], $RUR)).'" />'."\n";
	unset($currency, $currencyIterator, $allowedCurrency);

	$strTmp .= "</currencies>\n";

	fwrite($fp, $strTmp);
	unset($strTmp);

    $profile = '';
    if(preg_match("/\/(\d+)$/",$SETUP_FILE_NAME,$matches)){
        $profile=$matches[1];
    }

    if (is_array($YANDEX_EXPORT))
	{
        $strTmpCat = "";
        $strTmpOff = "";

	    /************ Sections **************/
        // выборка только активных разделов из инфоблока $IBLOCK_ID, в которых есть элементы
        // со значением свойства SRC, начинающееся с https://

        $cache = Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache("2592000", "dealer_xml", "/dealer/sections/"))
        {
            $strTmpCat = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            $intMaxSectionID = 0;
            $sectionIterator = Bitrix\Iblock\SectionTable::getList(array(
                'select' => array(
                    new Bitrix\Main\Entity\ExpressionField('MAX_ID', 'MAX(%s)', array('ID'))
                )
            ));
            if ($section = $sectionIterator->fetch())
                $intMaxSectionID = (int)$section['MAX_ID'];
            unset($section, $sectionIterator);
            $intMaxSectionID += 100000000;
            $maxSections = array();

            foreach ($YANDEX_EXPORT as $ykey => $yvalue) {
                $boolNeedRootSection = false;

                $yvalue = (int)$yvalue;
                if ($yvalue <= 0)
                    continue;

                $arIBlock = CIBlock::GetArrayByID($yvalue);
                if (empty($arIBlock) || !is_array($arIBlock))
                    continue;
                if ('Y' != $arIBlock['ACTIVE'])
                    continue;
                $boolRights = false;
                if ('E' != $arIBlock['RIGHTS_MODE']) {
                    $arRights = CIBlock::GetGroupPermissions($yvalue);
                    if (!empty($arRights) && isset($arRights[2]) && 'R' <= $arRights[2])
                        $boolRights = true;
                } else {
                    $obRights = new CIBlockRights($yvalue);
                    $arRights = $obRights->GetGroups(array('section_read', 'element_read'));
                    if (!empty($arRights) && in_array('G2', $arRights))
                        $boolRights = true;
                }
                if (!$boolRights)
                    continue;

                $filter = array("IBLOCK_ID" => $yvalue, "ACTIVE" => "Y", "IBLOCK_ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y");
                $db_acc = CIBlockSection::GetList(array("LEFT_MARGIN" => "ASC"), $filter, false, array('ID', 'IBLOCK_SECTION_ID', 'NAME'));

                $arAvailGroups = array();
                while ($arAcc = $db_acc->Fetch()) {
                    $arAcc['ID'] = (int)$arAcc['ID'];
                    $arAcc['IBLOCK_SECTION_ID'] = (int)$arAcc['IBLOCK_SECTION_ID'];
                    $strTmpCat .= '<category id="' . $arAcc['ID'] . '"' . ($arAcc['IBLOCK_SECTION_ID'] > 0 ? ' parentId="' . $arAcc['IBLOCK_SECTION_ID'] . '"' : '') . '>' . yandex_text2xml($arAcc['NAME'], true) . '</category>' . "\n";
                    $arAvailGroups[] = $arAcc['ID'];
                }
            }

            $cache->endDataCache($strTmpCat);
        }

        /************* Elements **************/

        $colors = \FUTU\Fhl::getList(HL_TSVET);
        $arResult = [];


        $cache = Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache("21600", "dealer_xml", "/dealer/elements/"))
        {
            $arResult = $cache->getVars();
        }
        elseif ($cache->startDataCache())
        {
            // full arr products
            $objProducts = \CIBlockElement::GetList(
                array(),
                array('IBLOCK_ID' => C_IB_PRODUCTS,'!ID' => 37462),//ID(подушка - 33575, скутер - 31463, ES1 - 31458)
                false,
                false,
                array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', "CATALOG_GROUP_".C_PRICE_OPT, 'DETAIL_TEXT')//, 'DETAIL_PAGE_URL', "PREVIEW_PICTURE", 'DETAIL_PICTURE', "CATALOG_GROUP_".C_PRICE_OPT
            );

            // check available SKU in IB
            $arInfo = CCatalogSKU::GetInfoByProductIBlock(C_IB_PRODUCTS);
            $hasOffers = (is_array($arInfo)) ? true : false;

            while ($objProduct = $objProducts->GetNextElement()) {
                //bug($objProduct);
                $fields = $objProduct->GetFields();

                // check exist SKU in Product
                $offersExist = CCatalogSKU::getExistOffers($fields['ID']);

                // if exist SKU - add to arrSKU
                if ($hasOffers && current($offersExist)) { // get sku

                    $res = CCatalogSKU::getOffersList(
                        $fields['ID'], // массив ID товаров
                        $iblockID = 0, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
                        $skuFilter = array(), // дополнительный фильтр предложений. по умолчанию пуст.
                        $sel_fields = array('CATALOG_AVAILABLE','DETAIL_PAGE_URL',"CATALOG_GROUP_".C_PRICE_OPT),  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
                        $propertyFilter = array() /* свойства предложений. имеет 2 ключа: ID - массив ID свойств предложений либо CODE - массив символьных кодов свойств предложений если указаны оба ключа, приоритет имеет ID*/
                    );

                    foreach ($res[$fields['ID']] as $k => $v) {
                        $el = \CIBlockElement::GetList(
                            array(),
                            array('IBLOCK_ID' => C_IB_OFFERS, 'ID' => [$k], '>PRICE' => 0, '=PRICE_TYPE' => C_PRICE_OPT),
                            false,
                            false,
                            array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', "CATALOG_GROUP_".C_PRICE_OPT, 'DETAIL_TEXT')
                        );
                        while ($i = $el->GetNextElement()) {
                            //if(is_null($fields["CATALOG_PRICE_".C_PRICE_OPT])) continue;
                            $f = $i->GetFields();
                            $f["IBLOCK_SECTION_ID"] = $objProduct->fields["IBLOCK_SECTION_ID"];
                            $f["DETAIL_TEXT"] = $objProduct->fields["DETAIL_TEXT"];
                            $f['PROPS'] = $i->GetProperties();
                            $arResult[$k] = $f;
                        }
                    }
                } else { // get simple product
                    if(is_null($fields["CATALOG_PRICE_".C_PRICE_OPT])) continue;
                    $fields['PROPS'] = $objProduct->GetProperties();
                    $arResult[$fields['ID']] = $fields;
                }
                unset($objProduct);
            }
            unset($objProducts);

            $cache->endDataCache($arResult);
        }

        foreach($arResult as $k=>$arAcc){
            $str_AVAILABLE = ($arAcc['CATALOG_AVAILABLE'] == 'Y' ? 'available="true"' : 'available="false"');

            $strTmpOff.= "<offer id='".$arAcc["ID"]."' ".$str_AVAILABLE.">\n";
            $strTmpOff.= "<url>".$usedProtocol.$SETUP_SERVER_NAME.htmlspecialcharsbx($arAcc["~DETAIL_PAGE_URL"]).(strstr($arAcc['DETAIL_PAGE_URL'], '?') === false ? '' : '')."</url>\n";
            $strTmpOff.= "<price>".$arAcc["CATALOG_PRICE_".C_PRICE_OPT]."</price>\n";
            $strTmpOff.= "<currencyId>RUR</currencyId>\n";
            $strTmpOff.= "<categoryId>".$arAcc["IBLOCK_SECTION_ID"]."</categoryId>\n";

            /*if (intval($arAcc["DETAIL_PICTURE"])>0 || intval($arAcc["PREVIEW_PICTURE"])>0)
            {
                $pictNo = intval($arAcc["DETAIL_PICTURE"]);

                $arPictInfo = CFile::GetFileArray($pictNo);
                if (is_array($arPictInfo))
                {
                    $strTmpOff.="<picture>".$arPictInfo['SRC']."</picture>\n";
                }
            }*/

            $barcode = (!empty($arAcc['PROPS']['SHTRIKHKOD_PROIZVODITELYA_1']['VALUE'])) ? $arAcc['PROPS']['SHTRIKHKOD_PROIZVODITELYA_1']['VALUE'] : $arAcc['PROPS']['SHTRIKHKOD_PROIZVODITELYA']['VALUE'];
            if($barcode) $strTmpOff.= "<barcode>".yandex_text2xml($barcode, true)."</barcode>\n";

            $strTmpOff.= "<name>".yandex_text2xml($arAcc["~NAME"], true)."</name>\n";

            if($SETUP_FILE_NAME  == "/bitrix/catalog_export/fl.xml") {
                // description
                if (!empty($arAcc["DETAIL_TEXT"])) $strTmpOff .= "<description>" . yandex_text2xml($arAcc["DETAIL_TEXT"], true) . "</description>\n";
                //photos
                if (is_array($arAcc['PROPS']['MORE_PHOTO']['VALUE'])) {
                    foreach ($arAcc['PROPS']['MORE_PHOTO']['VALUE'] as $img) {
                        $arPictInfo = CFile::GetFileArray($img);
                        if (is_array($arPictInfo)) {
                            $strTmpOff .= "<picture>" . $arPictInfo['SRC'] . "</picture>\n";
                        }
                    }
                }
            }

            $strTmpOff.= "<manufacturer_warranty>true</manufacturer_warranty>\n";

            $strTmpOff.= "<weight>".yandex_text2xml($arAcc["~CATALOG_WEIGHT"], true)."</weight>\n";

            $aq = ($arAcc["~CATALOG_QUANTITY"] > 50) ? 50 : $arAcc["~CATALOG_QUANTITY"];
            $strTmpOff.= "<param name='".$APPLICATION->ConvertCharset("Доступное количество",LANG_CHARSET,'windows-1251')."'>".$aq."</param>\n";

            // props
            $exclude = ['MORE_PHOTO',"CML2_LINK","VYGRUZHAT_NA_SAYT","vote_count","vote_sum","rating","NOMER_STELAZHA","CML2_TRAITS","CML2_TAXES","FORUM_TOPIC_ID","FORUM_MESSAGE_CNT",
                "KATEGORIYA_AVITO","PODKATEGORIYA_AVITO","REVIEW_HTML","PHOTOS_REVIEW"];
            foreach($arAcc['PROPS'] as $prop=>$v){
                if($prop == 'MORE_PHOTO') {

                }
                if(in_array($prop, $exclude) || empty($v['VALUE'])) continue;

                if($prop == 'TSVET') {// TSVET
                    $strTmpOff.= "<param name='".yandex_text2xml($v['NAME'],false)."'>".yandex_text2xml($colors[$v['VALUE']]['UF_NAME'],false)."</param>\n";
                }else
                    $strTmpOff.= "<param name='".yandex_text2xml($v['NAME'],false)."'>".yandex_text2xml($v['VALUE'],false)."</param>\n";
            }

            $strTmpOff.= "</offer>\n";
        }

        unset($arResult);
	}

	fwrite($fp, "<categories>\n");
	fwrite($fp, $strTmpCat);
	fwrite($fp, "</categories>\n");

	fwrite($fp, "<offers>\n");
	fwrite($fp, $strTmpOff);
	fwrite($fp, "</offers>\n");

	fwrite($fp, "</shop>\n");
	fwrite($fp, "</yml_catalog>\n");

	fclose($fp);
}

