<?
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/constants.php"))
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/constants.php");
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/debug.php"))
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/debug.php");
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/handlers.php"))
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/handlers.php");

include_once ($_SERVER['HOME'] . "/composer/vendor/autoload.php");

//include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");

// добавляем переменную FUTU, определяем группу пользователя = опт или розница
AddEventHandler("main", "OnBeforeProlog", ["OnBeforePrologHandler","init"]);

// деактивация пустых разделов
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", Array("iblockSectionEvents", "disableEmptySection"));

//Деактивируем простой товар без цены + не выгружать на сайт
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "disableElementsWithoutPrice"));

//Деактивируем товары из ремонта
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "disableElementsFromRepair"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("iblockElementsEvents", "disableElementsFromRepair"));

// удаляет пробелы в ifr ame
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "checkIframes"));

// добавляет при заказе в письмо инфу о доставке
AddEventHandler("sale", "OnOrderNewSendEmail", Array("emailEvents", "newOrderEvent"));

// вставляет OPT в название оптового заказа
AddEventHandler("sale", "OnBeforeOrderAccountNumberSet", Array("ordersEvents", "setOrderType"));

//Модифицируем вес товара для правильного расчета тарифа Сдек
AddEventHandler('ipol.sdek', 'onBeforeDimensionsCount', Array("ordersEvents", "handleGoods"));

// Регистрация оптовика
AddEventHandler('form', 'onBeforeResultAdd', Array('formEvents', "registerOpt"));

// Обновление Location в sale.order.ajax в зависимости от выбранного в header
AddEventHandler("sale", "OnSaleComponentOrderProperties", Array("SetLocation", "OnSaleComponentOrderProperties"));

//AddEventHandler("catalog", "OnSuccessCatalogImport1C", "upDateWiegth");
//AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", Array("iblockSectionEvents", "disableEmptySection"));

//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("iblockElementsEvents", "markElementsWithOffers"));
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "markElementsWithOffers"));
//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("iblockElementsEvents", "changeWeight"));
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "changeWeight"));
//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("iblockElementsEvents", "disableElementsWithoutPrice"));

//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("iblockElementsEvents", "changeSizesBrutto"));
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("iblockElementsEvents", "changeSizesBrutto"));

//AddEventHandler('sale', 'OnSalePayOrder', Array('saleOrderEvents', "unAuthShowRoomUsers"));



//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("ProductEvents", "setCanBuyZeroForOpt"));
//AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("ProductEvents", "setCanBuyZeroForOpt"));



class OnBeforePrologHandler{
    function init(){
        self::checkDevice();
        if($_SESSION['SESS_AUTH']['AUTHORIZED'] == "Y")self::checkUserGroup();
    }

    function checkDevice(){// 0 = desktop, 1 = mobile
        Bitrix\Main\Loader::includeModule('conversion');
        $detect = new \Bitrix\Conversion\Internals\MobileDetect;
        $_SESSION['FUTU']['device'] = ($detect->isMobile()) ? 1 : 0;
    }
    function checkUserGroup(){
        if(in_array("10",$_SESSION['SESS_AUTH']['GROUPS'])){
            $_SESSION['FUTU']['userOpt'] = "Y";
            setcookie('_aid', null, -1, '/');
        }
    }
}


class emailEvents
{

    function newOrderEvent($orderID, &$eventName, &$arFields)
    {
        $objDateTime = new DateTime();
        $objDateTime->add(new DateInterval('P1D'));

        $dInfo = '';
        $arOrder = CSaleOrder::GetByID($orderID);
        $order_props = CSaleOrderPropsValue::GetOrderProps($orderID);

        /*
       $f = fopen($_SERVER['DOCUMENT_ROOT'] . '/dump.txt', 'ab');
        fwrite($f, print_r($order_props, 1) . "\n==\n");
        fclose($f);
            */

        switch ($arOrder["DELIVERY_ID"]) {
            case 2:
                $dInfo = "Вы можете забрать Ваш заказ в любой день с 10:00 до 20:30 по адресу: г.Москва, ул. Зацепа 21";
                break;
            case 1:
                $dInfo = "Ваш заказ будет доставлен завтра " . $objDateTime->format("d-m-Y");
                break;
            case "sdek:courier":
                $dInfo = "Доставка в регионы осуществляется транспортными компаниями (в течение 2-14 дней). Отправка заказов производится в день заказа или на следующий день.";
                break;
            case "sdek:pickup":
                $dInfo = "Доставка в регионы осуществляется транспортными компаниями (в течение 2-14 дней). Отправка заказов производится в день заказа или на следующий день.";
                break;
        }

        $arFields["D_INFO"] = $dInfo;

    }
}

class formEvents
{

    function registerOpt($WEB_FORM_ID, &$arFields, &$arrVALUES)
    {
// действие обработчика распространяется только на форму с ID=10
        if ($WEB_FORM_ID == 10) {

            global $APPLICATION;
            $token = md5(rand());
            $FIOFORM = $arrVALUES["form_text_48"];
            $fio = explode(" ", $FIOFORM);
            $typeOf = "";
            switch ($arrVALUES["form_dropdown_TYPEOF"]) {
                case 53:
                    $typeOf = "Интернет-магазин";
                    break;
                case 54:
                    $typeOf = "Розничный магазин";
                    break;
                case 55:
                    $typeOf = "Дропшиппинг";
                    break;
            }
//Зарегистрируем пользователя
            $user = new CUser;
            $arUserFields = Array(
                "NAME" => $fio[1],
                "LAST_NAME" => $fio[0],
                "SECOND_NAME" => $fio[2],
                "EMAIL" => $arrVALUES["form_email_49"],
                "LOGIN" => $arrVALUES["form_email_49"],
                "LID" => "s1",
                "ACTIVE" => "Y",
                "GROUP_ID" => array(3, 6, 4),
                "PASSWORD" => $arrVALUES["form_password_50"],
                "CONFIRM_PASSWORD" => $arrVALUES["form_password_50"],
                "UF_YURPHONE" => $arrVALUES["form_text_51"],
                "UF_FACTADDRESS" => $arrVALUES["form_text_61"],
                "UF_YURADDRESS" => $arrVALUES["form_text_60"],
                "UF_OGRNIP" => $arrVALUES["form_text_59"],
                "UF_KPP" => $arrVALUES["form_text_58"],
                "UF_INN" => $arrVALUES["form_text_57"],
                "UF_YURNAME" => $arrVALUES["form_text_52"],
                "UF_ASSORT" => $arrVALUES["form_text_56"],
                "UF_TOA" => $typeOf,
                "UF_UNLOCK" => rand(),
            );
            $ID = $user->Add($arUserFields);
            if (intval($ID) > 0) {
                //Создадим профиль покупателя
                $arProfileFields = array(
                    "NAME" => $arrVALUES["form_text_52"],
                    "USER_ID" => intval($ID),
                    "PERSON_TYPE_ID" => 2
                );
                $PROFILE_ID = CSaleOrderUserProps::Add($arProfileFields);
                //если профиль создан
                if ($PROFILE_ID) {
                    //формируем массив свойств
                    $PROPS = Array(
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 8,
                            "NAME" => "Название компании",
                            "VALUE" => $arrVALUES["form_text_52"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 9,
                            "NAME" => "Юридический адрес",
                            "VALUE" => $arrVALUES["form_textarea_60"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 10,
                            "NAME" => "ИНН",
                            "VALUE" => $arrVALUES["form_text_57"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 11,
                            "NAME" => "КПП",
                            "VALUE" => $arrVALUES["form_text_58"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 12,
                            "NAME" => "Контактное лицо",
                            "VALUE" => $fio[0] . ' ' . $fio[1] . ' ' . $fio[2]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 13,
                            "NAME" => "E-Mail",
                            "VALUE" => $arrVALUES["form_email_49"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 14,
                            "NAME" => "Телефон",
                            "VALUE" => $arrVALUES["form_text_51"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 19,
                            "NAME" => "Адрес доставки",
                            "VALUE" => $arrVALUES["form_textarea_61"]
                        ),
                        array(
                            "USER_PROPS_ID" => $PROFILE_ID,
                            "ORDER_PROPS_ID" => 219,
                            "NAME" => "ОГРН/ОГРНИП",
                            "VALUE" => $arrVALUES["form_text_59"]
                        )
                    );
                    //добавляем значения свойств к созданному ранее профилю
                    foreach ($PROPS as $prop)
                        CSaleOrderUserPropsValue::Add($prop);
                    //	echo "<pre>"; print_r($arFields); echo "</pre>";
                    //	die;
                }

                $arEventFields = array(
                    "UF_YURPHONE" => $arrVALUES["form_text_51"],
                    "UF_FACTADDRESS" => $arrVALUES["form_text_61"],
                    "UF_YURADDRESS" => $arrVALUES["form_text_60"],
                    "UF_OGRNIP" => $arrVALUES["form_text_59"],
                    "UF_KPP" => $arrVALUES["form_text_58"],
                    "UF_INN" => $arrVALUES["form_text_57"],
                    "UF_YURNAME" => $arrVALUES["form_text_52"],
                    "UF_ASSORT" => $arrVALUES["form_text_56"],
                    "UF_TOA" => $typeOf,
                    "UF_UNLOCK" => $token,
                    "ID" => $ID,
                    "NAME" => $fio[1],
                    "LAST_NAME" => $fio[0],
                    "SECOND_NAME" => $fio[2],
                    "EMAIL" => $arrVALUES["form_email_49"],
                );
                $arEventFieldsOpt = array(
                    "NAME" => $fio[1],
                    "LAST_NAME" => $fio[0],
                    "SECOND_NAME" => $fio[2],
                    "EMAIL" => $arrVALUES["form_email_49"],
                    "LOGIN" => $arUserFields["LOGIN"],
                    "PASSWORD" => $arUserFields["PASSWORD"]
                );
                CEvent::Send("UNLOCK_OPTUSER", "s1", $arEventFields, "Y", 946);
                CEvent::Send("USER_INFO", "s1", $arEventFieldsOpt, "Y", 948);

//LocalRedirect( "/auth/success.php");
            } else
                $APPLICATION->ThrowException($user->LAST_ERROR);
            echo "<script>$('#li1, #panel1').removeClass('active');
		$('#li2, #panel2').addClass('active');
		$('#li2, #panel2').removeClass('fade in');</script>";
        }
    }
}

class ordersEvents
{

    function handleGoods(&$arOrderGoods)
    {
        foreach ($arOrderGoods as $key => $arGood) {
            $TP = CCatalogSku::GetProductInfo($arGood['PRODUCT_ID']);
            if ($TP) {
                $ID = $TP["ID"];
            } else {
                $ID = $arGood['PRODUCT_ID'];
            }
            $elt = CIBlockElement::GetList(array(), array('ID' => $ID), false, false, array('ID', 'PROPERTY_DLINA_UPAKOVKI_MM', 'PROPERTY_SHIRINA_UPAKOVKI_MM', 'PROPERTY_VYSOTA_UPAKOVKI_MM', 'PROPERTY_DLINA_USTROYSTVA_MM', 'PROPERTY_SHIRINA_USTROYSTVA_MM', 'PROPERTY_VYSOTA_USTROYSTVA_MM'))->Fetch();
            if ($elt["PROPERTY_DLINA_UPAKOVKI_MM_VALUE"] > 0) {
                $length = $elt["PROPERTY_DLINA_UPAKOVKI_MM_VALUE"];
            } else {
                $length = $elt["PROPERTY_DLINA_USTROYSTVA_MM_VALUE"];
            }
            if ($elt["PROPERTY_SHIRINA_UPAKOVKI_MM_VALUE"] > 0) {
                $width = $elt["PROPERTY_SHIRINA_UPAKOVKI_MM_VALUE"];
            } else {
                $width = $elt["PROPERTY_SHIRINA_USTROYSTVA_MM_VALUE"];
            }
            if ($elt["PROPERTY_VYSOTA_UPAKOVKI_MM_VALUE"] > 0) {
                $height = $elt["PROPERTY_VYSOTA_UPAKOVKI_MM_VALUE"];
            } else {
                $height = $elt["PROPERTY_VYSOTA_USTROYSTVA_MM_VALUE"];
            }
            $arOrderGoods[$key]['DIMENSIONS'] = array(
                "LENGTH" => $length,
                "WIDTH" => $width,
                "HEIGHT" => $height,
            );
        }
    }

    function setOrderType($orderId)
    {
        $arOrder = CSaleOrder::GetByID($orderId);
        $groups = CUser::GetUserGroup($arOrder['USER_ID']);
        if (in_array(10, $groups)) {
            return sprintf('%s-%s', 'OPT', $orderId);
        }
    }

}

/*
class saleOrderEvents {
//разавторизовывает текущего пользователя после оплаты, при авторизации из шоурума(не считая админов) 
	
	function unAuthShowRoomUsers($ID, $val) {
		global $USER;
		$orderData = CSaleOrder::GetByID($ID);
		//echo "<pre>"; print_r($orderData); echo "</pre>";
		//die;
		
		if( self::getRealIP() == '195.91.135.58' && !$USER->IsAdmin() && $val == 'Y' )
		if ($USER->IsOnLine($orderData["USER_ID"],10)) { 
			$USER->Logout();
			}
	}

	function getRealIP() {
	   	static $realip;

	   	if (isset($_SERVER)) {
	      	if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
	         	$realip = current(preg_grep(
	            	"/^(10|172\\.16|192\\.168)\\./",
	            	array_map('trim', explode(',', $_SERVER['HTTP_X_FORWARDED_FOR'])),
	            	PREG_GREP_INVERT
	         	));
	      	}
	      	$realip = $realip?:@$_SERVER['HTTP_CLIENT_IP']?:@$_SERVER['REMOTE_ADDR']?:NULL;
	   	}
	   	return $realip?:$realip = '0.0.0.0';
	}

}
*/

/*
function upDateWiegth()
{
if (CModule::IncludeModule("catalog")) {
	$db_res = CCatalogProduct::GetList(array(),array(),false, false);
	while ($ar_res = $db_res->GetNext())
	{
		$arSelect = Array("ID", "PROPERTY_VES_KG"); //получаем вес товара из свойства
		$arFilter = Array("ID"=>$ar_res["ID"]);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNext())
				{
					if ($ob['PROPERTY_VES_KG_VALUE']) {
						$wightResult = $ob['PROPERTY_VES_VALUE']*1000;
						if	(CCatalogProduct::Update($ar_res["ID"], array ("WEIGHT" => $wightResult)) == true) {
						echo "Вес товара ".$ar_res["ID"]." обновлен";
						} else {
						echo "Произошла ошибка при обновление веса товара";
						}
					}
				}


	}
}
} */

class iblockSectionEvents
{
    // создаем обработчик события  Деактивируем пуcтые разделы
    function disableEmptySection(&$arFields)
    {
        $activeElements = CIBlockSection::GetSectionElementsCount($arFields['ID'], Array("CNT_ACTIVE" => "Y"));
        if ($activeElements <= 0 || $arFields['ID'] == 1547 || $arFields['ID'] == 1631) {
            $arFields["ACTIVE"] = "N";
        } else {
            $arFields["ACTIVE"] = "Y";
        }
    }

}

class iblockElementsEvents
{

    // Помечаем товар с торговыми предложениями под выгрузку на Я.маркет
    function markElementsWithOffers(&$arFields)
    {
        $offersExist = CCatalogSKU::getExistOffers($arFields['ID']);

        if ($offersExist[$arFields['ID']] > 0) {
            $arFields["PROPERTY_VALUES"]["21640"][0]["VALUE"] = "124816";
        } else {
            $arFields["PROPERTY_VALUES"]["21640"][0]["VALUE"] = "";
        }
    }

    function disableElementsWithoutPrice(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == C_IB_PRODUCTS) {
            $ar_res = CCatalogProduct::GetByID($arFields['ID']);
            $basePrice = CPrice::GetBasePrice($arFields['ID'], false, false);

            if ($ar_res["TYPE"] == 1) {
                $i = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE" => "VYGRUZHAT_NA_SAYT"));
                if ($i_res = $i->Fetch())
                    $active = $i_res["VALUE"];
                if ($active == "126633")
                    $toActive = 1;

                $arFields["ACTIVE"] = "Y";
                if ((!$toActive) || (!$basePrice["PRICE"])) {
                    $arFields["ACTIVE"] = "N";
                }

            }
        }
        //echo "<pre>"; print_r($arFields["PROPERTY_VALUES"]["22508"][0]["VALUE"]); echo "</pre>";
        //die;
    }

    // Модифицируем вес для Я.Маркет
    function changeWeight(&$arFields)
    {
        $ar_res = CCatalogProduct::GetByID($arFields["ID"]);
        if (!empty($ar_res["WEIGHT"])) {
            $arFields["PROPERTY_VALUES"]["21643"]["n0"]["VALUE"] = ($ar_res["WEIGHT"] / 1000);
        }
    }
    // деактивируем товры из ремонта
    function disableElementsFromRepair(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == C_IB_PRODUCTS || $arFields["IBLOCK_ID"] == C_IB_OFFERS) {
            if(strripos($arFields['NAME'],"№00НФ") || strripos($arFields['NAME'],"№НФНФ")){
                $arFields["ACTIVE"] = "N";
            }
        }
    }


    /*	//Заведем в торговый каталог размеры упаковки либо размеры устройства при отсутствии данных
        function changeSizesBrutto($arFields) {
            if (CModule::IncludeModule("catalog")) {
                //Ширина
                $key = key($arFields["PROPERTY_VALUES"]["21442"]);
                if ($arFields["PROPERTY_VALUES"]["21442"][$key]["VALUE"] > 0) {
                    $width = $arFields["PROPERTY_VALUES"]["21442"][$key]["VALUE"];

                } elseif ($arFields["PROPERTY_VALUES"]["21445"][$key]["VALUE"] > 0){
                    $width = $arFields["PROPERTY_VALUES"]["21445"][$key]["VALUE"];
                }
                //Длина
                $key = key($arFields["PROPERTY_VALUES"]["21441"]);
                if ($arFields["PROPERTY_VALUES"]["21441"][$key]["VALUE"] > 0) {
                    $length = $arFields["PROPERTY_VALUES"]["21441"][$key]["VALUE"];
                } elseif ($arFields["PROPERTY_VALUES"]["21444"][$key]["VALUE"] > 0){
                    $length = $arFields["PROPERTY_VALUES"]["21444"][$key]["VALUE"];
                }
                //Высота
                $key = key($arFields["PROPERTY_VALUES"]["21443"]);
                if ($arFields["PROPERTY_VALUES"]["21443"][$key]["VALUE"] > 0){
                    $height = $arFields["PROPERTY_VALUES"]["21443"][$key]["VALUE"];
                } elseif ($arFields["PROPERTY_VALUES"]["21446"][$key]["VALUE"] > 0){
                    $height = $arFields["PROPERTY_VALUES"]["21446"][$key]["VALUE"];
                }
                CCatalogProduct::Add(array("ID" => $arFields["ID"], "WIDTH" => $width, "LENGTH" => $length, "HEIGHT" => $height));
                    $ar_res = CCatalogProduct::GetByID($arFields["ID"]);
                echo "<pre>"; print_r($ar_res); echo "</pre>";
               //die;
            }
        }*/


    /*
    function checkIframes(&$arFields)
    {
        if ($arFields["IBLOCK_ID"] == 211) {
            $key = key($arFields["PROPERTY_VALUES"]["23036"]);
            $arFields["PROPERTY_VALUES"]["23036"][$key]["VALUE"] = str_replace('ifr ame', 'iframe', $arFields["PROPERTY_VALUES"]["23036"][$key]["VALUE"]);
        }

        if ($arFields["IBLOCK_ID"] == 15) {
            $arFields["DETAIL_TEXT"] = str_replace('ifr ame', 'iframe', $arFields["DETAIL_TEXT"]);
        }
    }*/

}


?>