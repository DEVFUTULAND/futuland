<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if( !CModule::IncludeModule("iblock") || !CModule::IncludeModule('catalog') || !CModule::IncludeModule("nkhost.phpexcel") )
	return;
use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Catalog,
    Bitrix\Catalog\ProductTable,
    Bitrix\Iblock\ElementTable;

global $USER;
global $PHPEXCELPATH;      

require_once ($PHPEXCELPATH . '/PHPExcel/IOFactory.php');
$PRODUCT_IBLOCK_ID = (!empty($arParams['IBLOCK_ID'])) ? $arParams['IBLOCK_ID'] :211;
$SKU_IBLOCK_ID = (!empty($arParams['SKU_IBLOCK_ID'])) ? $arParams['SKU_IBLOCK_ID'] :212;

if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, $arNavigation, $arrFilter, $pagerParameters)))
{
	$tree = CIBlockSection::GetTreeList(
	    $arFilter=Array('IBLOCK_ID' => $PRODUCT_IBLOCK_ID),
	    $arSelect=Array()
	);
	while($section = $tree->GetNext()) {
		$arResult['SECTIONS'][] = $section;
	}
	
	$this->setResultCacheKeys(array('SECTION'));

	$this->includeComponentTemplate();
}

$sections = Context::getCurrent()->getRequest()->get("SECTIONS");

if(!empty($sections)) {
	
	$arSelect = Array("ID", "NAME", "CATALOG_QUANTITY", "PROPERTY_CML2_ARTICLE", "CATALOG_AVAILABLE");
	$arFilter = Array("IBLOCK_ID"=>IntVal($PRODUCT_IBLOCK_ID), "ACTIVE"=>"Y","SECTION_ID"=>$sections,"INCLUDE_SUBSECTIONS"=>"Y");
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	$arElements = array();
	$arOffersIds = array();
	while($ar_fields = $res->GetNext()) {
		$price = CCatalogProduct::GetOptimalPrice($ar_fields['ID'], 1, $USER->GetUserGroupArray(), 'N');
		
		$item = [
			'ID' => $ar_fields['ID'],
			'NAME' => $ar_fields['NAME'],
			'CML2_ARTICLE' => $ar_fields['PROPERTY_CML2_ARTICLE_VALUE'],
			'CATALOG_QUANTITY' => $ar_fields['CATALOG_QUANTITY'],
			'PRICE' =>  $price['PRICE']['PRICE']
		];
		if($ar_fields['CATALOG_AVAILABLE'] == 'Y') {
			$arElements[] = $item;
		} else {
			$arOffersIds[] = intval($item['ID']);	
		}
	}
	
	if(!empty($arOffersIds)) {
		$offers = CCatalogSKU::getOffersList(
					$arOffersIds,
					0,
					array("CATALOG_AVAILABLE"=>"Y"),
					array('NAME','CATALOG_QUANTITY','PROPERTY_CML2_ARTICLE'),
					array()
				);
		foreach($offers as $elems) {
			foreach($elems as $elem) {

				$price = CCatalogProduct::GetOptimalPrice($elem['ID'], 1, $USER->GetUserGroupArray(), 'N');
				$item = [
					'ID' => $elem['ID'],
					'NAME' => $elem['NAME'],
					'CML2_ARTICLE' => $elem['PROPERTY_CML2_ARTICLE_VALUE'],
					'CATALOG_QUANTITY' => $elem['CATALOG_QUANTITY'],
					'PRICE' =>  $price['PRICE']['PRICE']
				];
				$arElements[] = $item;
			}
		}
	}
	
	require_once($PHPEXCELPATH.'/PHPExcel.php');
	require_once($PHPEXCELPATH.'/PHPExcel/Writer/Excel5.php');
	
	$xls = new PHPExcel();
	$xls->setActiveSheetIndex(0);
	$sheet = $xls->getActiveSheet();
	$sheet->setTitle('Заказ');
	
	$sheet->setCellValue("A1", 'Название');
	$sheet->setCellValue("B1", 'Артикул');
	$sheet->setCellValue("C1", 'Доступное количество');
	$sheet->setCellValue("D1", 'Количество к заказу');
	$sheet->setCellValue("E1", 'Цена');
	$sheet->setCellValue("F1", 'Стоимость');
	$sheet->getStyle('A1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('A1')->getFill()->getStartColor()->setRGB('EEEEEE');
	$sheet->getStyle('B1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('B1')->getFill()->getStartColor()->setRGB('EEEEEE');
	$sheet->getStyle('C1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('C1')->getFill()->getStartColor()->setRGB('EEEEEE');
	$sheet->getStyle('D1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('D1')->getFill()->getStartColor()->setRGB('EEEEEE');
	$sheet->getStyle('E1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('E1')->getFill()->getStartColor()->setRGB('EEEEEE');
	$sheet->getStyle('F1')->getFill()->setFillType(
	    PHPExcel_Style_Fill::FILL_SOLID);
	$sheet->getStyle('F1')->getFill()->getStartColor()->setRGB('EEEEEE');

	$sheet->getColumnDimension('A')->setAutoSize(true);
	$sheet->getColumnDimension('B')->setAutoSize(true);
	$sheet->getColumnDimension('C')->setAutoSize(true);
	$sheet->getColumnDimension('D')->setAutoSize(true);
	$sheet->getColumnDimension('E')->setAutoSize(true);
	$sheet->getColumnDimension('F')->setAutoSize(true);
	
	for ($i=0; $i < count($arElements) ; $i++) { 
		$sheet->setCellValueByColumnAndRow(0, $i+2, $arElements[$i]['NAME']);
		$sheet->setCellValueByColumnAndRow(1, $i+2, $arElements[$i]['CML2_ARTICLE']);
		$sheet->setCellValueByColumnAndRow(2, $i+2, $arElements[$i]['CATALOG_QUANTITY']);
		$sheet->setCellValueByColumnAndRow(3, $i+2, 0);
		$sheet->setCellValueByColumnAndRow(4, $i+2, $arElements[$i]['PRICE']);
		$sheet->setCellValueByColumnAndRow(5, $i+2,'=D'.($i+2).'*E'.($i+2));
	}
	
	//вывод общей стоимости заказа
	/*$sheet->setCellValueByColumnAndRow(0, $i+4, 'Стоимость заказа');
	$sheet->setCellValueByColumnAndRow(1, $i+4,'=SUM(F2:F'.($i+1).')');*/
	
	header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
	header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
	header ( "Cache-Control: no-cache, must-revalidate" );
	header ( "Pragma: no-cache" );
	header ( "Content-type: application/vnd.ms-excel" );
	header ( "Content-Disposition: attachment; filename=matrix123.xls" );
	ob_end_clean();
	// Выводим содержимое файла
	$objWriter = new PHPExcel_Writer_Excel5($xls);
	$objWriter->save('php://output');
	exit;

}

$file = Context::getCurrent()->getRequest()->get("file");
if(empty($file))
	return;

$fid = CFile::SaveFile($_FILES['file'], "orders");
$filePath = CFile::GetPath($fid);

$file = $_SERVER["DOCUMENT_ROOT"].$filePath;

if (!file_exists($file)) {
	return;
}

$xls = PHPExcel_IOFactory::load($file);
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
$empty_value = 0;
for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {
	$nColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
  	for ($j = 0; $j < $nColumn; $j++) {
  		$arProducts[$i][$j] = $sheet->getCellByColumnAndRow($j, $i)->getValue();
  	}
}

//проверка не пустой ли заказ
$emptyOrder = true;
for( $i = 2; $i <= count($arProducts); $i++) {
	
	if($arProducts[$i][3] <= 0 || empty($arProducts[$i][0]))
		continue;
	
	$emptyOrder = false;
	break;
}
if($emptyOrder)
	return;

$siteId = Context::getCurrent()->getSite();
$currencyCode = CurrencyManager::getBaseCurrency();

$order = Order::create($siteId, $USER->isAuthorized() ? $USER->GetID() : 539);
$order->setPersonTypeId(1);
$order->setField('CURRENCY', $currencyCode);

$propertyCollection = $order->getPropertyCollection();
$somePropValue = $propertyCollection->getItemByOrderPropertyId(216);
$somePropValue->setValue("Оптовый");

//устанавливаем свойства
$name = $USER->GetFullName();
$email = $USER->GetEmail();
$propertyCollection = $order->getPropertyCollection();
$phoneProp = $propertyCollection->getPhone();
$phoneProp->setValue($email);
$nameProp = $propertyCollection->getPayerName();
$nameProp->setValue($name);

//создаем корзину
$basket = Basket::create($siteId);
//добавление в корзину товаров
for( $i = 2; $i <= count($arProducts); $i++) {
	
	if($arProducts[$i][3] <= 0 || empty($arProducts[$i][0]))
		continue;
	//поиск по товарам
	$filter = Array("PROPERTY_CML2_ARTICLE" => $arProducts[$i][1], "IBLOCK_ID" => $PRODUCT_IBLOCK_ID, "CATALOG_AVAILABLE"=>"Y");
	$arItem = CIBlockElement::GetList(
		Array("SORT"=>"ASC"),
		$filter,
		false,
		false,
		Array()
	);

	$item = $arItem->Fetch();

	//если нет - поиск по ТП
	if(!$item)
		$filter["IBLOCK_ID"] = $SKU_IBLOCK_ID;

	$arItem = CIBlockElement::GetList(
		Array("SORT"=>"ASC"),
		$filter,
		false,
		false,
		Array()
	);

	$item = $arItem->Fetch();

	if(!$item)
		continue;

	if($arProducts[$i][2]>=$arProducts[$i][3]) 
		$quantity = intval($arProducts[$i][3]);
	else
		$quantity = intval($arProducts[$i][2]);

	$item = $basket->createItem('catalog', $item['ID']);
	$item->setFields(array(
	    'QUANTITY' => $quantity,
	    'CURRENCY' => $currencyCode,
	    'LID' => $siteId,
	    'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
	));
}
//привязываем корзину к новому заказу
$order->setBasket($basket);

//сохраняем
$order->doFinalAction(true);
$result = $order->save();
$orderId = $order->getId();


