<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-checkbox-tree.js');?>
<div>
<h2>Загрузка заказа</h2>
<form name="order" method="POST" action="" enctype="multipart/form-data">
	<input type="file" name="file" value=""><br>
	<button type="submit">Отправить</button>
</form>
<?$this->SetViewTarget('left_menu');?>
<div class="bx_filter">
<div class="bx_filter_section">
<h2>Выбор разделов</h2>
<?if(empty($_REQUEST['SECTIONS'])):?>
	<form name="sections" method="POST" action="">
		<ul class="checkbox-tree root">
		<?$currentLevel = 1;?>
		<?foreach($arResult['SECTIONS'] as $key => $section):?>

			<li >
			
			<?if ($section["DEPTH_LEVEL"] < $arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"]):?>
				<ul>
			<?endif;?>
			
			<input id="<?=$section["ID"];?>" 
				class="checkbox" 
				type="checkbox" 
				name="SECTIONS[]" 
				value="<?=$section["ID"];?>"
				<?(in_array($section["ID"], $_REQUEST["SECTIONS"])) ? 'checked' : '' ?> />
			<label for="<?=$section["ID"];?>"><?=$section["NAME"]?></label>

			<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] < $section["DEPTH_LEVEL"] ):?>
				<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] - $section["DEPTH_LEVEL"] == 2):?>
					</ul>
				<?endif;?>
				</ul>
			<?endif;?>			
			</li>			
			<?if($arResult['SECTIONS'][$key+1]["DEPTH_LEVEL"] - $section["DEPTH_LEVEL"] == 1 || count($arResult['SECTIONS']) == $key+1):?>
				</li>
			<?endif;?>
		<?endforeach;?>
		</ul>
		<button type="submit">Отправить</button>
	</form>
	</div>
	</div>
<?$this->EndViewTarget();?>
	
<script type="text/javascript">
  jQuery(document).ready(function(){
    var cbTree = $('.checkbox-tree').checkboxTree({
      checkChildren : true,
      singleBranchOpen: true,
      
    });
    $('#tree-expand').on('click', function(e) {
      cbTree.expandAll();
    });
    $('#tree-collapse').on('click', function(e) {
      cbTree.collapseAll();
    });
    $('#tree-default').on('click', function(e) {
      cbTree.defaultExpand();
    });
    $('#tree-select-all').on('click', function(e) {
      cbTree.checkAll();
    });
    $('#tree-deselect-all').on('click', function(e) {
      cbTree.uncheckAll();
    });
    $('.checkbox-tree').on('checkboxTicked', function(e) {
      var checkedCbs = $(e.currentTarget).find("input[type='checkbox']:checked");
     // console.log('checkbox tick', checkedCbs.length);
    });
  });
  </script>	
<?endif;?>
</div>