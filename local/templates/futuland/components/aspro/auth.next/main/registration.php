<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? \Bitrix\Main\Localization\Loc::loadMessages(__FILE__); ?>
<? $APPLICATION->AddChainItem(GetMessage("TITLE")); ?>
<? $APPLICATION->SetTitle(GetMessage("TITLE")); ?>
<? $APPLICATION->SetPageProperty("TITLE_CLASS", "center"); ?>
    <style type="text/css">
        .left-menu-md, body .container.cabinte-page .maxwidth-theme .left-menu-md, .right-menu-md, body .container.cabinte-page .maxwidth-theme .right-menu-md {
            display: none !important;
        }

        .content-md {
            width: 100%;
        }
    </style>
<? global $USER, $APPLICATION;
$regOptUser = ($_GET['opt'] == 'Y') ? true : false;

if (!$USER->IsAuthorized()) {
    ?>
    <div class="tabs">
        <ul class="nav nav-tabs">
            <? if($regOptUser){?>
                <li id="li2" class="active"><a data-toggle="tab" href="#panel2">Оптовый клиент</a></li>
            <?}else{?>
                <li id="li1" class="active"><a data-toggle="tab" href="#panel1">Розничный клиент</a></li>
            <?}?>
        </ul>

        <div class="tab-content">
            <? if($regOptUser){?>
                <div id="panel2" class="tab-pane fade in active">
                    <div class="module-form-block-wr registraion-page">
                        <div class="form-block border_block">
                            <div class="wrap_md">
                                <div class="main_info iblock">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:form.result.new",
                                        "opt_registration",
                                        Array(
                                            "CACHE_TIME" => "3600",
                                            "CACHE_TYPE" => "N",
                                            "CHAIN_ITEM_LINK" => "",
                                            "CHAIN_ITEM_TEXT" => "",
                                            "COMPONENT_TEMPLATE" => "inline",
                                            "EDIT_URL" => "result_edit.php",
                                            "IGNORE_CUSTOM_TEMPLATE" => "N",
                                            "LIST_URL" => "result_list.php",
                                            "SEF_MODE" => "Y",
                                            "SUCCESS_URL" => "/auth/success.php",
                                            "USE_EXTENDED_ERRORS" => "Y",
                                            "VARIABLE_ALIASES" => array("WEB_FORM_ID" => "WEB_FORM_ID", "RESULT_ID" => "RESULT_ID",),
                                            "WEB_FORM_ID" => "10"
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?}else{?>
                <div id="panel1" class="tab-pane fade in active">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.register",
                        "main",
                        Array(
                            "USER_PROPERTY_NAME" => "",
                            "SHOW_FIELDS" => array("LOGIN", "LAST_NAME", "NAME", "SECOND_NAME", "EMAIL", "PERSONAL_PHONE"),
                            "REQUIRED_FIELDS" => array("NAME", "PERSONAL_PHONE", "EMAIL"),
                            "AUTH" => "Y",
                            "USE_BACKURL" => "Y",
                            "SUCCESS_PAGE" => "",
                            "SET_TITLE" => "N",
                            "USER_PROPERTY" => array("UF_TYPE")
                        )
                    ); ?>
                    <?
                    $rsEvents = GetModuleEvents("form", "onSubmitForm");
                    while ($arEvent = $rsEvents->Fetch()) {
                        echo "<pre>";
                        print_r($arEvent);
                        echo "</pre>";
                    }
                    ?>
                </div>
            <?}?>
        </div>
    </div>
<? } else {
    LocalRedirect($arParams["SEF_FOLDER"]);
} ?>