<?global $USER;?>
<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("form-block".$arParams["WEB_FORM_ID"]);?>
<?if($USER->IsAuthorized()):?>
	<script type="text/javascript">
	$(document).ready(function() {
		try{
			$('.form.<?=$arResult["arForm"]["SID"]?> input[data-sid=CLIENT_NAME], .form.<?=$arResult["arForm"]["SID"]?> input[data-sid=FIO], .form.<?=$arResult["arForm"]["SID"]?> input[data-sid=NAME]').val('<?=$USER->GetFullName()?>');
			$('.form.<?=$arResult["arForm"]["SID"]?> input[data-sid=EMAIL]').val('<?=$USER->GetEmail()?>');
		}
		catch(e){
		}
	});
	</script>
<?endif;?>
<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("form-block".$arParams["WEB_FORM_ID"], "");?>

<link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/css/suggestions.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@19.8.0/dist/js/jquery.suggestions.min.js"></script>

<script>

    var phone = $('input[data-sid="PHONE"]');

    var im = new Inputmask("+7(999) 999-99-99", { "clearIncomplete": true });
    im.mask(phone);

    $(('input[data-sid="INN"]')).suggestions({
        token: "96460351f71e2f6aad256ad407603b2a479a1fa9",
        type: "PARTY",
        onSelect: function(suggestion) {
            var ip = suggestion.data.type == 'INDIVIDUAL';

            var arData = {
                'FIO': ip ? suggestion.data.name.full : suggestion.data.management.name,
                'ONAME': suggestion.data.name.short_with_opf,
                'KPP': ip ? "" : suggestion.data.kpp,
                'OGRN': suggestion.data.ogrn,
                'URADDRESS': suggestion.data.address.unrestricted_value,
                'INN': suggestion.data.inn,
            };

            $.each(arData, function(key, value) {
                $(('input[data-sid="'+key+'"]')).val(value);
            });

        }
    });
</script>