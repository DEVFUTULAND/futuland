<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<style>
    .brand_wrapper{border-top:unset;}
    .brand_wrapper a{display: block;}
    .brand_wrapper a span.name{position: absolute;top:0;display: block;left: 0;right: 0;margin:0 auto;color:#222;}
</style>
<?if($arResult["ITEMS"]):?>
	<div class="brand_wrapper">
		<div class="row">
			<div class="brands_list">
				<?foreach( $arResult["ITEMS"] as $arItem ){
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					//bug($arItem);
					?>
					<div class="col-xs-6 col-md-4 col-lg-2 item">
						<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']?>">
                                <span class="name"><?=$arItem["NAME"]?></span>
								<?if( is_array($arItem["PREVIEW_PICTURE"]) ){?>
									<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=($arItem["PREVIEW_PICTURE"]["ALT"]?$arItem["PREVIEW_PICTURE"]["ALT"]:$arItem["NAME"]);?>" title="<?=($arItem["PREVIEW_PICTURE"]["TITLE"]?$arItem["PREVIEW_PICTURE"]["TITLE"]:$arItem["NAME"]);?>" />
								<?}?>
							</a>
						</div>
					</div>
				<?}?>
			</div>
		</div>
	</div>
<?endif;?>