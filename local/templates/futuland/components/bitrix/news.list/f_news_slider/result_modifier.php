<?php
/**
 * Created by PhpStorm.
 * User: Jager
 * Date: 10.07.19
 * Time: 10:18
 */
foreach($arResult['ITEMS'] as $key => $arItem) {
    //bug($arItem['PROPERTIES']['BUTTON_TEXT']['VALUE']);
    $arResult['ITEMS'][$key]['OUT_LINK'] = false;
    if($arItem['PROPERTIES']['REDIRECT']['VALUE'] !== ''){
        $arResult['ITEMS'][$key]['OUT_LINK'] = $arItem['PROPERTIES']['REDIRECT']['VALUE'];
    }

    if ($arItem['PROPERTIES']['LINK_GOODS']['VALUE'] > 0) {
        $iterator = \CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID' => C_IB_PRODUCTS, 'ID' => $arItem['PROPERTIES']['LINK_GOODS']['VALUE']),
            false,
            false,
            array()
        );
        //bug($iterator);
        while($ar = $iterator->GetNext()) {
            //bug($ar);
            $arResult['ITEMS'][$key]['OUT_LINK'] = $ar['DETAIL_PAGE_URL'];
        }
    }
    //bug($arResult['ITEMS'][$key]['OUT_LINK']);
    if($arResult['ITEMS'][$key]['OUT_LINK'] == false)  $arResult['ITEMS'][$key]['OUT_LINK'] = $arItem['DETAIL_PAGE_URL'];

    $arResult['ITEMS'][$key]['OUT_LINK_TEXT'] = ($arItem['PROPERTIES']['BUTTON_TEXT']['VALUE'] !== '') ? $arItem['PROPERTIES']['BUTTON_TEXT']['VALUE'] : "Заказать";
}