<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true );?>
<div id="main_news_line" class="container-fluid">
    <div class="row">
        <?foreach($arResult["ITEMS"] as $arItem){//bug($arItem['OUT_LINK_TEXT']);?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <?if( is_array($arItem["PREVIEW_PICTURE"]) ){
                $renderImg = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 330, "height" => 168), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
                <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="col-xs-2 news_el">
                    <a href="<?=$arItem['OUT_LINK']?>">
                        <img src="<?=$renderImg["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
                        <div class="text-bottom">
                            <?if($arItem['DISPLAY_ACTIVE_FROM']){?>
                                <span class="date"><?=$arItem['DISPLAY_ACTIVE_FROM'];?></span>
                            <?}?>
                            <span class="name"><?=mb_strimwidth($arItem['NAME'], 0, 58, "...")?></span>
                            <span class="btn btn-primary btn-sm text-center"><?=$arItem['OUT_LINK_TEXT']?></span>
                        </div>
                    </a>
                </div>
            <?}?>
        <?}?>
        <div class="col-xs-2 all_news"><a href="<?=$arParams['ALL_URL']?>"><?=$arParams['TITLE_BLOCK_ALL']?> <span>>></span></a></div>
    </div>
</div>