<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<style>
    .brands_slider_wrapp.newslider {min-height: 110px;margin-bottom: 5rem;}
    .brands_slider_wrapp.newslider .flex-viewport {height: auto;}
    .brands_slider_wrapp.newslider li {line-height: 2rem;}
    .brands_slider_wrapp.newslider a *{display: inline-block;font-weight: bolder;}
    .brands_slider_wrapp.newslider a .date{color:gray; margin-bottom:5px;}
    .brands_slider_wrapp.newslider a .name{color:#222;}
    .brands_slider_wrapp.newslider a:hover .name{color:var(--primary2);}
</style>
<div class="brands_slider_wrapp flexslider loading_state clearfix newslider" data-plugin-options='{"animation": "slide", "directionNav": true,"minItems":5,"maxItems":5, "move": 1, "itemMargin":30, "controlNav" :false, "animationLoop": true, "slideshow": false, "counts": [5]}'>
	<ul class="brands_slider slides">
		<?foreach($arResult["ITEMS"] as $arItem){?>
			<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>
            <li>
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <?if($arItem['DISPLAY_ACTIVE_FROM']){?>
                        <span class="date"><?=$arItem['DISPLAY_ACTIVE_FROM'];?></span>
                    <?}?>
                    <span class="name"><?=mb_strimwidth($arItem['NAME'], 0, 58, "...")?></span>
                </a>
            </li>
		<?}?>
	</ul>
</div>