<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use FUTU\Fglobal;?>
<?$this->setFrameMode(true);?>
<div class="maxwidth-theme">
	<!--div class="news_block_title">
		<div class="top_block">
			<?/*$title_block=($arParams["TITLE_BLOCK"] ? $arParams["TITLE_BLOCK"] : GetMessage('NEWS_TITLE'));
			$title_block_all=($arParams["TITLE_BLOCK_ALL"] ? $arParams["TITLE_BLOCK_ALL"] : GetMessage('ALL_NEWS'));
			$url=($arParams["ALL_URL"] ? $arParams["ALL_URL"] : SITE_DIR."company/news/");
			*/?>
			<div class="title_block"><?=$title_block;?></div>
			<a href="<?=SITE_DIR.$url;?>"><?=$title_block_all;?></a>
			<div class="clearfix"></div>
		</div>
	</div-->
	<div class="sub_container fixed_wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="banners-small blog">
					<div class="items row lazy_main_news">
						<?foreach($arResult['ITEMS'] as $key => $arItem):?>
							<?
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							
							// preview image
							$bImage = (is_array($arItem['PREVIEW_PICTURE']) && $arItem['PREVIEW_PICTURE']['SRC']);
							$imageSrc = ($bImage ? $arItem['PREVIEW_PICTURE']['SRC'] : false);

							// use detail link?
							$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);

							$isWideBlock = (isset($arItem['CLASS_WIDE']) && $arItem['CLASS_WIDE']);
							$hasWideBlock = (isset($arItem['CLASS']) && $arItem['CLASS']);?>
							<div class="<?=((isset($arItem['CLASS']) && $arItem['CLASS']) ? $arItem['CLASS'] : 'col-md-3 col-sm-4');?>">
								<div class="item shadow animation-boxs <?=($isWideBlock ? 'wide-block' : '')?> <?=($hasWideBlock ? '' : 'normal-block')?>"  id="<?=$this->GetEditAreaId($arItem['ID']);?>">
									<div class="inner-item">
										<?if($bImage){
                                            $renderImg = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 375, "height" => 191), BX_RESIZE_IMAGE_PROPORTIONAL, false);?>
											<div class="image shine">
												<?if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?endif;?>
												<img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=$renderImg['src']?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                     alt="<?=($bImage ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME'])?>" title="<?=($bImage ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME'])?>" />
												<?if($bDetailLink):?></a><?endif;?>
											</div>
										<?}?>
										<div class="title">
											<?if($bDetailLink):?><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?endif;?>
												<span><?=$arItem['NAME']?></span>
											<?if($bDetailLink):?></a><?endif;?>
											<?if($arItem['PREVIEW_TEXT'] && ($isWideBlock || !$bImage)):?>
												<div class="prev_text-block"><?=$arItem['PREVIEW_TEXT'];?></div>
											<?endif;?>
											<?if($arItem['DISPLAY_ACTIVE_FROM']):?>
												<div class="date-block"><?=$arItem['DISPLAY_ACTIVE_FROM'];?></div>
											<?endif;?>
										</div>
									</div>
								</div>
							</div>
						<?endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>