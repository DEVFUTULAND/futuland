<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
$this->setFrameMode(true);
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.flexslider-min.js');
if($arResult["ITEMS"]){?>
    <?$TARGET_NAME = false;
    if($arParams["PARENT_SECTION"] == "1872"){
        $TARGET_NAME = "main_slider";
    }?>
        <script>
            $(window).load(function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    pauseOnAction: true,
                    pauseOnHover: true,
                    touch: true,
                    controlNav: true,
                    direction: "horizontal",
                    directionNav: true,
                    slideshow: true,
                    slideshowSpeed: 4000,
                    animationSpeed: 1000,

                });
            });
        </script>
        <style>
            body .top_slider_wrapp{margin:0;}
            body .top_slider_wrapp .flexslider{max-height: 420px;}
            body .top_slider_wrapp .flexslider .flex-control-nav{top:-50px;}
            body .top_slider_wrapp .flexslider ul.slides li{padding-left:unset;}
            body .top_slider_wrapp .flexslider .flex-control-nav li{padding:unset;margin:6px;height: 10px;}
        </style>
    <div class="top_slider_wrapp">
        <div class="flexslider">
            <ul class="slides">
                <?foreach($arResult["ITEMS"] as $arItem){?>
                    <li>
                        <a href="<? if(is_array($arItem["DISPLAY_PROPERTIES"]['LINK'])) echo $arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']; else echo '/'; ?>"
                           <?if($TARGET_NAME){?>onclick="ym(50513497, 'reachGoal', '<?=$TARGET_NAME?>'); return true;"<?}?> >
                            <img class="img img-responsive" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>" />
                        </a>
                    </li>
                <?}?>
            </ul>
        </div>
    </div>
<?}?>