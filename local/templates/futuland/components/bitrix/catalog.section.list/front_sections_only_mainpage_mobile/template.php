<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>

<?if ($arResult['SECTIONS']) { ?>
        <div class="list items">
            <div id="main_cat_m" class="row">
                <?
                $intCurrentDepth = 1;
                $boolFirst = true;
                foreach ($arResult['SECTIONS'] as &$arSection){

                    $level1 = ($arSection['RELATIVE_DEPTH_LEVEL'] === 1) ? true : false;

                    if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL']) {
                        if (0 < $intCurrentDepth)
                            echo "\n", str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']), '<div class=""><ul>';
                    } elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL']) {
                        if (!$boolFirst)
                            echo '</li>';
                    } else {
                        while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL']) {
                            echo '</li>', "\n", str_repeat("\t", $intCurrentDepth), '</ul>', "\n", str_repeat("\t", $intCurrentDepth - 1);
                            $intCurrentDepth--;
                        }
                        echo str_repeat("\t", $intCurrentDepth - 1), '</li>';
                    }
                    echo(!$boolFirst ? "\n" : ''), str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);?>


                        <li id="<?= $arSection['ID'],"-",$arSection['RELATIVE_DEPTH_LEVEL']?>" <?=$level1 ? "class='col-xs-6 col-sm-4'":""?>>
                            <?=($level1) ? "<a href=".$arSection["SECTION_PAGE_URL"]." class='front'>" : ""?>
                            <?if($level1){?>
                                <div class="img shine">
                                    <?if($arSection["PICTURE"]["SRC"]){?>
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"]["ID"], array( "width" => 100, "height" => 100 ), BX_RESIZE_IMAGE_EXACT, true );?>
                                        <img class="img" height="100" width="100" src="<?=$img["src"]?>" alt="<?=($arSection["PICTURE"]["ALT"] ? $arSection["PICTURE"]["ALT"] : $arSection["NAME"])?>" title="<?=($arSection["PICTURE"]["TITLE"] ? $arSection["PICTURE"]["TITLE"] : $arSection["NAME"])?>" />
                                    <?}?>
                                </div>
                            <?}?>
                                <div class="name"><span><?=$arSection["NAME"]; ?></span></div><?=($level1) ? "</a>" : ""?><?

                        $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                        $boolFirst = false;
                        }
                        unset($arSection);
                        while ($intCurrentDepth > 1) {
                            echo '</li>', "\n", str_repeat("\t", $intCurrentDepth), '</ul></div>', "\n", str_repeat("\t", $intCurrentDepth - 1);
                            $intCurrentDepth--;
                        }
                        if ($intCurrentDepth > 0) {
                            echo '</div>', "\n";
                        }?>
            </div>
        </div>

<?
} ?>
