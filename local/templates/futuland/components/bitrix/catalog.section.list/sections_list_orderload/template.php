<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/bootstrap-checkbox-tree.js');?>
<?if($arResult["SECTIONS"]){?>
<div class="bx_filter">
<div class="bx_filter_section">
<form name="sections" action="" method="POST" class="">
<span style="font-weight:bold; padding-left:80px">Категории</span>
        <ul class="checkbox-tree root" style="margin-left:20px;">
		<?foreach( $arResult["SECTIONS"] as $arItems ):?>
		<li>
		<input id="<?=$arItems["ID"]?>" class="checkbox" type="checkbox" name="SECTIONS[]" value="<?=$arItems["ID"]?>" <?if (in_array($arItems["ID"], $_REQUEST["ID"])):?> checked <?endif;?> />
		<label for="<?=$arItems["ID"]?>" style="padding-left:25px"><?=$arItems["NAME"];?></label>
				<?if ($arItems["SECTIONS"]):?>
				<ul>
						<?foreach ($arItems["SECTIONS"] as $arChild):?> 
						<li style="margin-left:16px">
						<input id="<?=$arChild["ID"];?>" class="checkbox" type="checkbox" name="SECTIONS[]" value="<?=$arChild["ID"];?>" <?if (in_array($arChild["ID"], $_REQUEST["ID"])):?> checked <?endif;?> />
						<label for="<?=$arChild["ID"];?>"><?=$arChild["NAME"];?></label>
						
						</li>
						<?endforeach;?>
				</ul>
				<?endif;?>
		</li>
		<?endforeach;?>
		</ul>
<button type="submit" class="btn btn-default load" style="width:50%; margin-left:18px; display:none">Скачать шаблон</button>
</form>
		<div style="margin:5px 0 15px 18px">
          <button id="tree-select-all" class="btn btn-default">Выбрать все</button>
          <button id="tree-deselect-all" class="btn btn-default">Очистить</button>
        </div>
</div>
</div>
<script type="text/javascript">
  jQuery(document).ready(function(){
	
    var cbTree = $('.checkbox-tree').checkboxTree({
      checkChildren : true,
      singleBranchOpen: true,
      
    });
    $('#tree-expand').on('click', function(e) {
      cbTree.expandAll();
    });
    $('#tree-collapse').on('click', function(e) {
      cbTree.collapseAll();
    });
    $('#tree-default').on('click', function(e) {
      cbTree.defaultExpand();
    });
    $('#tree-select-all').on('click', function(e) {
      cbTree.checkAll();
    });
    $('#tree-deselect-all').on('click', function(e) {
      cbTree.uncheckAll();
    });
    $('.checkbox-tree').on('checkboxTicked', function(e) {
      var checkedCbs = $(e.currentTarget).find("input[type='checkbox']:checked");
	  $('.load').fadeIn();
	  if (checkedCbs.length == 0)
	  $('.load').hide();
     console.log('checkbox tick', checkedCbs.length);
    });
	
	$('.load').on('click', function() {
		$('.step2').fadeIn();
		
	});
  });
  </script>
 
<?}?>