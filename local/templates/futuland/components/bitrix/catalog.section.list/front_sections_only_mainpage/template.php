<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
<script>
    $(".shifter").flip({
        axis: 'y',
        trigger: 'hover'
    });
</script>
<?
if ($arResult['SECTIONS']) { ?>

        <?
        if ($arParams["TITLE_BLOCK"] || $arParams["TITLE_BLOCK_ALL"]) { ?>
            <div class="top_block">
                <h3 class="title_block"><?= $arParams["TITLE_BLOCK"]; ?></h3>
                <a href="<?= SITE_DIR . $arParams["ALL_URL"]; ?>"><?= $arParams["TITLE_BLOCK_ALL"]; ?></a>
            </div>
        <?
        } ?>
        <div class="list items">
            <div id="main_cat" class="row">
                <?
                $intCurrentDepth = 1;
                $boolFirst = true;
                foreach ($arResult['SECTIONS'] as &$arSection){

                    $level1 = ($arSection['RELATIVE_DEPTH_LEVEL'] === 1) ? true : false;
                    if($level1) {
                        $mainSection['NAME'] = $arSection['NAME'];
                        $mainSection['SECTION_PAGE_URL'] = $arSection['SECTION_PAGE_URL'];
                    }

                    if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL']) {
                        if (0 < $intCurrentDepth)
                            echo "\n", str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']), '<div class="back">'.($mainSection ? '<a class="name header" href="'. $mainSection["SECTION_PAGE_URL"].'">'.$mainSection["NAME"].'</a>' : '').'<ul>';
                    } elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL']) {
                        if (!$boolFirst)
                            echo '</li>';
                    } else {
                        while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL']) {
                            echo '</li>', "\n", str_repeat("\t", $intCurrentDepth), '</ul>', "\n", str_repeat("\t", $intCurrentDepth - 1);
                            $intCurrentDepth--;
                        }
                        echo str_repeat("\t", $intCurrentDepth - 1), '</li>';
                    }
                    echo(!$boolFirst ? "\n" : ''), str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);?>


                        <li id="<?= $arSection['ID'],"-",$arSection['RELATIVE_DEPTH_LEVEL']?>" <?=$level1 ? "class='col5 shifter'":""?>>
                            <?=($level1) ? "<div class='front'>" : ""?>
                            <?if($level1){?>
                                <div class="img shine">
                                    <?if($arSection["PICTURE"]["SRC"]){?>
                                        <?$img = CFile::ResizeImageGet($arSection["PICTURE"]["ID"], array( "width" => 200, "height" => 200 ), BX_RESIZE_IMAGE_EXACT, true );?>
                                        <img src="<?=$img["src"]?>" alt="<?=($arSection["PICTURE"]["ALT"] ? $arSection["PICTURE"]["ALT"] : $arSection["NAME"])?>" title="<?=($arSection["PICTURE"]["TITLE"] ? $arSection["PICTURE"]["TITLE"] : $arSection["NAME"])?>" />
                                    <?}else{?>
                                        <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=SITE_TEMPLATE_PATH?>/images/catalog_category_noimage.png" alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>" /></a>
                                    <?}?>
                                </div>
                            <?}?>
                                <div class="name"><a href="<? echo $arSection["SECTION_PAGE_URL"]; ?>"><? echo $arSection["NAME"]; ?><?
                                if ($arParams["COUNT_ELEMENTS"]) {
                                    ?> <span>(<? echo $arSection["ELEMENT_CNT"]; ?>)</span><?
                                }
                                    ?></a></div><?=($level1) ? "</div>" : ""?><?

                        $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                        $boolFirst = false;
                        }
                        unset($arSection);
                        while ($intCurrentDepth > 1) {
                            echo '</li>', "\n", str_repeat("\t", $intCurrentDepth), '</ul></div>', "\n", str_repeat("\t", $intCurrentDepth - 1);
                            $intCurrentDepth--;
                        }
                        if ($intCurrentDepth > 0) {
                            echo '</div>', "\n";
                        }
                    /*foreach($arResult['SECTIONS'] as &$arSection){?>
                        <div class="col-md-2 col-sm-4 col-xs-6">
                            <div class="item" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                                <?if ($arParams["SHOW_SECTION_LIST_PICTURES"]!="N"):?>
                                    <div class="img shine">
                                        <?if($arSection["PICTURE"]["SRC"]):?>
                                            <?$img = CFile::ResizeImageGet($arSection["PICTURE"]["ID"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
                                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="<?=($arSection["PICTURE"]["ALT"] ? $arSection["PICTURE"]["ALT"] : $arSection["NAME"])?>" title="<?=($arSection["PICTURE"]["TITLE"] ? $arSection["PICTURE"]["TITLE"] : $arSection["NAME"])?>" /></a>
                                        <?elseif($arSection["~PICTURE"]):?>
                                            <?$img = CFile::ResizeImageGet($arSection["~PICTURE"], array( "width" => 120, "height" => 120 ), BX_RESIZE_IMAGE_EXACT, true );?>
                                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=$img["src"]?>" alt="<?=($arSection["PICTURE"]["ALT"] ? $arSection["PICTURE"]["ALT"] : $arSection["NAME"])?>" title="<?=($arSection["PICTURE"]["TITLE"] ? $arSection["PICTURE"]["TITLE"] : $arSection["NAME"])?>" /></a>
                                        <?else:?>
                                            <a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="thumb"><img src="<?=SITE_TEMPLATE_PATH?>/images/catalog_category_noimage.png" alt="<?=$arSection["NAME"]?>" title="<?=$arSection["NAME"]?>" /></a>
                                        <?endif;?>
                                    </div>
                                <?endif;?>
                                <div class="name">
                                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>" class="dark_link"><?=$arSection['NAME'];?></a>
                                </div>
                            </div>
                        </div>
                    <?}*/ ?>
            </div>
        </div>

<?
} ?>
