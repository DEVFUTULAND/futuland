<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Page\Asset;
CJSCore::Init(array("jquery2"));
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui-1.9.2.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/vendor/hummingbird-treeview/hummingbird-treeview.js");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/vendor/hummingbird-treeview/hummingbird-treeview.css");

//bug($_REQUEST);?>

<? $this->setFrameMode( true ); ?>
<div class="bx_filter_sections">
    <form name="categorys" action="/catalog" method="GET" class="">
        <hr>
        <div class="buttons">
            <button id="tree-check-all" class="btn btn-primary">Выбрать все</button>
            <button id="tree-uncheck-all" class="btn btn-primary">Очистить</button>
            <button  type="submit" class="btn btn-primary">Применить</button>
        </div>
        <hr>
        <ul id="treeview" class="hummingbird-base hummingbird-treeview">
        <?foreach( $arResult["SECTIONS"] as $arItems ){?>
            <li><i class="fa fa-plus"></i>
                <label for="<?=$arItems["ID"]?>" >
                    <input id="<?=$arItems["ID"]?>" class="checkbox" type="checkbox" name="<?=$arItems["ID"]?>" value="Y" <?if (array_key_exists($arItems["ID"], $_REQUEST)):?> checked <?endif;?> />
                    <?=$arItems["NAME"];?></label>
                <?if ($arItems["SECTIONS"]):?>
                <ul>
                    <?foreach ($arItems["SECTIONS"] as $arChild){?>
                        <li>
                            <label>
                                <input id="<?=$arChild["ID"];?>" class="hummingbirdNoParent" type="checkbox" name="<?=$arChild["ID"];?>" value="Y" <?if (array_key_exists($arChild["ID"], $_REQUEST)):?> checked <?endif;?> />
                            <?=$arChild["NAME"];?></label>
                        </li>
                    <?}?>
                </ul>
                <?endif;?>
            </li>
        <?}?>
        </ul>
    </form>
</div>

<script>
    $(function() {
        var catTree = $("#treeview");
        catTree.hummingbird();

        $("#tree-check-all").click(function() {
            catTree.hummingbird("checkAll");
        });

        $("#tree-uncheck-all").click(function() {
            catTree.hummingbird("uncheckAll");
        });
    });
</script>
