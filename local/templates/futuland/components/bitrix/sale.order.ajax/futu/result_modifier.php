<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

if(!empty($arResult["ORDER_ID"])){
    CModule::IncludeModule('sale');
    $res = CSaleBasket::GetList(array(), array("ORDER_ID" => $arResult["ORDER_ID"]));
    $json=array();
    while ($arItem = $res->Fetch()) {
        $json[] = $arItem['PRODUCT_ID'];
    }

    $arResult['ORDER_ITEMS_ARRAY'] = json_encode($json);
}

