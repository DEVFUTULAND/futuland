<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use FUTU\Fglobal;

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//cache PHOTOS_REVIEW_BLOCK
if($arResult["PROPERTIES"]["PHOTOS_REVIEW"]["VALUE"]){
    $arResult['REVIEW_PHOTOS_BLOCK'] = '';

    foreach($arResult["PROPERTIES"]["PHOTOS_REVIEW"]["VALUE"] as $k=>$v) {
        if($_SESSION['FUTU']['device'] == 1){
            $renderImg = CFile::ResizeImageGet($v, Array("width" => 350, "height" => 800), BX_RESIZE_IMAGE_PROPORTIONAL, false);
        }else
            $renderImg = CFile::ResizeImageGet($v, Array("width" => 1314, "height" => 3000), BX_RESIZE_IMAGE_PROPORTIONAL, false);

        $arResult['REVIEW_PHOTOS_BLOCK'] .= "<img class='img img-responsive' v-bx-lazyload data-lazyload-dont-hide data-lazyload-src=".$renderImg['src']." src=".Fglobal::Lazy()['preload']." alt='Фото обзора ". $k. " ". $arResult['NAME']. " data-lazyload-error-src=".Fglobal::Lazy()['error']."'><br>";
    }
}