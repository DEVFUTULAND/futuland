$(function() {
    //lazy load
    var lazy_el = ['.lazy_main_news', '.lazy_f1','.lazy_f2','.lazy_f3','.lazy_f4','.lazy_tizers','.lazy_review','.lazy_cat_item'];

    for (var i = 0; i < lazy_el.length; i++) {
        BX.Vue.create({
            el: lazy_el[i]
        });
    }

    //fixedheader
    // When the user scrolls the page, execute myFunction
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("fixedheader");

    // Get the offset position of the navbar
    var sticky = header.offsetTop;

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    $(document).on('click', '.contacts-wrap .contact-title', function() {
        if($(window).width() <= 767) {
            $(this).closest('.contacts-wrap').find('.contact-info').slideToggle();
        }
    });

    $(document).on('click', '.road-description-wrap h4', function() {
        var point = $(this).closest('.road-description-wrap');
        point.find('p').slideToggle();
        point.next().slideToggle();
    });
});
