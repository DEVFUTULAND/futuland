<div class="mobilemenu-v1 scroller">
	<div class="wrap">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"top_mobile",
			Array(
				"COMPONENT_TEMPLATE" => "top_mobile",
				"MENU_CACHE_TIME" => "3600000",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_USE_GROUPS" => "N",
				"MENU_CACHE_GET_VARS" => array(
				),
				"DELAY" => "N",
				"MAX_LEVEL" => \Bitrix\Main\Config\Option::get("aspro.next", "MAX_DEPTH_MENU", 2),
				"ALLOW_MULTI_SELECT" => "Y",
				"ROOT_MENU_TYPE" => "top_content_multilevel",
				"CHILD_MENU_TYPE" => "left",
				"CACHE_SELECTED_ITEMS" => "N",
				"ALLOW_MULTI_SELECT" => "Y",
				"USE_EXT" => "Y"
			)
		);?>
		<?
		// show regions
		CNext::ShowMobileRegions();?>

		<? global $USER;
		if(!$USER::IsAuthorized()):?>
			<div class="menu middle">
				<ul>
					<li>
						<a rel="nofollow" class="dark-color" href="/auth/registration/?register=yes&opt=Y&backurl=/#panel2">
							<i class="rollover svg inline svg-inline-cabinet" aria-hidden="true"></i>
							<span>Купить оптом</span>
						</a>
					</li>
				</ul>
			</div>
			<style type="text/css">
				i.rollover {
			    	background: url(<?=SITE_TEMPLATE_PATH?>/images/rubl_black.png); /* Путь к файлу с исходным рисунком  */
			    	background-size: cover;
			    	width: 17px!important;
			    	height: 17px!important;
			   }
			</style>
		<?endif;?>

		<?// show cabinet item
		CNext::ShowMobileMenuCabinet();

		// show basket item
		CNext::ShowMobileMenuBasket();

		// use module options for change contacts
		CNext::ShowMobileMenuContacts();
		?>
		<?$APPLICATION->IncludeComponent(
			"aspro:social.info.next",
			"mobile",
			array(
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600000",
				"CACHE_GROUPS" => "N",
				"COMPONENT_TEMPLATE" => ".default"
			),
			false
		);?>
	</div>
</div>