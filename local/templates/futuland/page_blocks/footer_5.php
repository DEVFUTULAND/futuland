<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use FUTU\Fglobal;?>
<div class="footer_inner <?= ($arTheme["SHOW_BG_BLOCK"]["VALUE"] == "Y" ? "fill" : "no_fill"); ?> footer-light">
    <div class="bottom_wrapper">
        <div class="maxwidth-theme items">
            <div class="row bottom-middle">
                <div class="col-md-12">
                    <div id="mainrow" class="row">
                        <div class="col-sm-3">
                            <div class="bottom-menu">
                                <div class="items">
                                    <div class="item-link">
                                        <div class="item">
                                            <div class="title">
                                                <span class="h4">Компания Futuland</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap">
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <?if($_SESSION['FUTU']['userOpt'] === 'Y') {?>
                                                        <a href="/company/">О компании</a>
                                                    <?} else {?>
                                                        <a href="/company/trogatelnyy-magazin-futuland.php">Наш магазин</a>
                                                    <?}?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <a href="/company/news/">Все новости</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="bottom-menu">
                                <div class="items">
                                    <div class="item-link">
                                        <div class="item">
                                            <div class="title">
                                                <span class="h4">Правовая информация</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap">
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <a href="/help/warranty/">Условия гарантии</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <a href="/include/licenses_detail.php">Соглашение</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="bottom-menu">
                                <div class="items">
                                    <div class="item-link">
                                        <div class="item">
                                            <div class="title">
                                                <span class="h4">Сотрудничество</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wrap">
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <a target="_blank" href="https://giroservis.ru/">Сервисный центр</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-link">
                                            <div class="item">
                                                <div class="title">
                                                    <a rel="nofollow" class="personal-link animate-load" data-event="jqm" data-param-type="auth" data-param-backurl="/auth/" data-param-user="opt" data-name="auth" href="/personal/">Оптовые закупки</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 lazy_f1">
                            <a class="" href="https://market.yandex.ru/shop/391564/reviews" target="_blank">
                                <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="/local/templates/futuland/images/yandex-market-4.jpg" src="<?=Fglobal::Lazy()['preload']?>"
                                     id="ym5" alt="Маркет 4 звезды" title="Маркет 4 звезды" class="" />
                            </a>
                        </div>
                    </div>

                    <div class="row endrow">
                        <div class="col-md-12 outer-wrapper">
                            <div class="inner-wrapper row img-line">
                                <div class="col-xs-12 col-md-6">
                                    <div class="social-icons">
                                        <!-- noindex -->
                                        <ul class="lazy_f2">
                                            <li class="dzen">
                                                <a href="https://zen.yandex.ru/futuland" target="_blank" rel="nofollow" title="YandexZen">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/dzen.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                         alt="dzen"/>
                                                </a>
                                            </li>
                                            <li class="instagram">
                                                <a href="https://www.instagram.com/futuland/" target="_blank" rel="nofollow" title="Instagram">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/instagram.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                         alt="instagram"/>
                                                </a>
                                            </li>
                                            <li class="vk">
                                                <a href="https://vk.com/futulandru" target="_blank" rel="nofollow" title="Вконтакте">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/vk.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                    alt="vk"/>
                                                </a>
                                            </li>
                                            <li class="ok">
                                                <a href="https://ok.ru/dk?st.cmd=anonymGroupForum&st.gid=55997628612626" target="_blank" rel="nofollow" title="OK">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/ok.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                         alt="ok"/>
                                                </a>
                                            </li>
                                            <li class="twitter">
                                                <a href="https://twitter.com/gorkomps" target="_blank" rel="nofollow" title="Twitter">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/twitter.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                    alt="twitter"/>
                                                </a>
                                            </li>
                                            <li class="youtube">
                                                <a href="https://www.youtube.com/channel/UCIoDN4mUjX32yeS0Wg3sQYg" target="_blank" rel="nofollow" title="Youtube">
                                                    <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/social/youtube-new.jpg"?>" src="<?=Fglobal::Lazy()['preload']?>"
                                                         alt="youtube"/>
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- /noindex -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="logo-pay lazy_f3">
                                        <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/mobile_pay.png"?>" src="<?=Fglobal::Lazy()['preload']?>" alt="pay"/>
                                    </div>
                                </div>
                            </div>
                            <div class="inner-wrapper row">
                                <?if($_SESSION['FUTU']['device'] === 0){?>
                                    <div class="copy-block col-md-12">
                                        <div class="pay_system_icons pull-left lazy_f4">
                                            <img v-bx-lazyload data-lazyload-dont-hide data-lazyload-error-src="<?=Fglobal::Lazy()['error']?>" data-lazyload-src="<?=SITE_TEMPLATE_PATH."/images/pay-sprite.png"?>" src="<?=Fglobal::Lazy()['preload']?>" alt="pay_icons"/>
                                        </div>
                                    </div>
                                <?}?>
                            </div>
                            <div class="inner-wrapper row">
                                <div class="copy-block col-md-12">
                                    <div class="copy pull-left">
                                        <?$APPLICATION->IncludeFile(SITE_DIR . "include/footer/copy/copyright.php", Array(), Array(
                                                    "MODE" => "php",
                                                    "NAME" => "Copyright",
                                                    "TEMPLATE" => "include_area.php",
                                                )
                                            );?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>