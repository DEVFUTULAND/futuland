<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
global $arTheme, $arRegion, $USER;
$arRegions = CNextRegionality::getRegions();
if($arRegion)
	$bPhone = ($arRegion['PHONES'] ? true : false);
else
	$bPhone = ((int)$arTheme['HEADER_PHONES'] ? true : false);
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
?>
<div id="fixedheader" class="header-v4 header-wrapper">
	<div class="logo_and_menu-row">
		<div class="logo-row">
			<div class="maxwidth-theme">
				<div class="row">
					<div class="col-sm-4">
						<div class="col-xs-8">
                            <a href="/" class="middlepos"><img class="img img-responsive" src="<?=SITE_TEMPLATE_PATH?>/images/logo4.jpeg" width="300" height="52" alt="Интернет магазин электротранспорта и цифровой техники в Москве FUTULAND"></a>
						</div>
                        <div class="col-xs-4">
                            <div class="inner-table-block">
                                <?php
                                    $dynamicArea = new \Bitrix\Main\Page\FrameStatic("my_dynamic");
                                    $dynamicArea->setAnimation(true);
                                    $dynamicArea->setStub("заглушка");
                                    $dynamicArea->setContainerID("div-geoloc");
                                    $dynamicArea->startDynamicArea();
                                        $APPLICATION->IncludeComponent(
                                            "futuland:geoloc",
                                            "",
                                            Array()
                                        );
                                    $dynamicArea->finishDynamicArea();
                                ?>
                            </div>
                        </div>
					</div>

					<div class="search_wrap col-sm-4">
						<div class="search-block inner-table-block">
							<?$APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR."include/top_page/search.title.catalog.php",
									"EDIT_TEMPLATE" => "include_area.php"
								)
							);?>
						</div>
					</div>
                    <div class="col-sm-4">
                        <div class="col-sm-5">
                            <?if($bPhone):?>
                                <div class="wrap_icon inner-table-block">
                                    <div class="phone-block">
                                        <span class="n1">Бесплатно</span>
                                        <div class="phone">
                                            <i class="svg svg-phone"></i>
                                            <a rel="nofollow" href="tel:88005552268">8 (800) 555-22-68</a>
                                        </div>
                                        <?if($arTheme['SHOW_CALLBACK']['VALUE'] == 'Y'):?>
                                            <div class="callback-block">
                                                <span class="animate-load twosmallfont colored" data-event="jqm" data-param-form_id="CALLBACK" data-name="callback"><?=GetMessage("CALLBACK")?></span>
                                            </div>
                                        <?endif;?>
                                    </div>
                                </div>
                            <?endif?>
                        </div>
                        <div class="col-sm-7 personal-link">
                            <?if($USER::IsAuthorized()){?>
                                <div class="pull-right">
                                    <div class="wrap_icon inner-table-block">
                                        <a rel="nofollow" title="Мой кабинет" class="personal-link dark-color" href="/personal/"><i class="svg inline big svg-inline-cabinet" aria-hidden="true" title="Мой кабинет"><svg xmlns="http://www.w3.org/2000/svg" width="21.03" height="21" viewBox="0 0 21.03 21">
                                                    <defs>
                                                        <style>
                                                            .uscls-1 {
                                                                fill: #222;
                                                                fill-rule: evenodd;
                                                            }
                                                        </style>
                                                    </defs>
                                                    <path data-name="Rounded Rectangle 107" class="uscls-1" d="M1425.5,111a6.5,6.5,0,1,1-6.5,6.5A6.5,6.5,0,0,1,1425.5,111Zm0,2a4.5,4.5,0,1,1-4.5,4.5A4.5,4.5,0,0,1,1425.5,113Zm8.35,19c-1.09-2.325-4.24-4-6.85-4h-3c-2.61,0-5.79,1.675-6.88,4h-2.16c1.11-3.448,5.31-6,9.04-6h3c3.73,0,7.9,2.552,9.01,6h-2.16Z" transform="translate(-1414.97 -111)"></path>
                                                </svg>
                                            </i><span class="wrap"><span class="name"></span><span class="title">Мой кабинет</span></span></a><!-- /noindex -->		<!--'end_frame_cache_header-auth-block1'-->
                                    </div>
                                </div>
                            <?}else{?>
                                <div class="col-sm-6">
                                    <div class="wrap_icon">
                                        <a rel="nofollow" title="Мой кабинет" id="opt" class="personal-link dark-color animate-load " data-event="jqm" data-param-type="auth" data-param-backurl="/auth/" data-name="auth" href="/personal/" width="24" height="24">
                                            <div class="col-sm-12 line1">
                                                <i class="box_retail float-l"></i>
                                                <span class="name float-l">Розница</span>
                                                <div class="clearBoth"></div>
                                            </div>
                                            <div class="col-sm-12"><span class="title">Мой кабинет</span></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="wrap_icon">
                                        <a rel="nofollow" title="Мой кабинет" id="opt" class="personal-link dark-color animate-load " data-event="jqm" data-param-type="auth" data-param-backurl="/auth/" data-param-user="opt" data-name="auth" href="/personal/" width="24" height="24">
                                            <div class="col-sm-12 line1">
                                                <i class="box_opt float-l"></i>
                                                <span class="name float-l">Опт</span>
                                                <div class="clearBoth"></div>
                                            </div>
                                            <div class="col-sm-12"><span class="title">Мой кабинет</span></div>
                                        </a>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div id="ftopmenu" class="menu-row middle-block bg<?=strtolower($arTheme["MENU_COLOR"]["VALUE"]);?>">
		<div class="maxwidth-theme">
			<div class="row">
				<div class="col-md-12">
					<div class="menu-only">
						<nav class="mega-menu sliced">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default",
								array(
									"COMPONENT_TEMPLATE" => ".default",
									"PATH" => SITE_DIR."include/menu/menu.top.php",
									"AREA_FILE_SHOW" => "file",
									"AREA_FILE_SUFFIX" => "",
									"AREA_FILE_RECURSIVE" => "Y",
									"EDIT_TEMPLATE" => "include_area.php"
								),
								false, array("HIDE_ICONS" => "Y")
							);?>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line-row visible-xs"></div>
</div>