<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>
<div class="col-md-9 col-sm-9 big">
<h3 class="title_block lg green">Регистрация завершена успешно!</h3>
<p>Ваш статус оптового клиента находится на рассмотрении, результат рассмотрения Вашего статуса будет выслан Вам по E-MAIL.</p>
<a href="/auth/" class="btn btn-default">Авторизация</a>
</div>
<style>
.green {
	color:#228c22;
}
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>