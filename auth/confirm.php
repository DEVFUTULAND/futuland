<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>
<?
if (CModule::IncludeModule("main")):
$secret = $_GET["secret"];
$ID = $_GET["ID"];
$rsUser = CUser::GetByID($ID);
$arUser = $rsUser->Fetch();
if (md5($arUser["UF_UNLOCK"]) === $secret) {
CUser::SetUserGroup($ID, 10);
}
$arGroups = CUser::GetUserGroup($ID);
$arGroups[] = 10;
CUser::SetUserGroup($ID, $arGroups);
$arGroups = CUser::GetUserGroup($ID);
?>
<?if (in_array(10, $arGroups)):?>
<div class="col-md-9 col-sm-9 big">
<h3 class="title_block lg green">Присвоение статуса оптового клиента успешно завершено!</h3>
</div>
<style>
.green {
	color:#228c22;
}
</style>
<?
$arEventFields = array(
  "NAME"              => $arUser["NAME"],
  "LAST_NAME"         => $arUser["LAST_NAME"],
  "SECOND_NAME"       => $arUser["SECOND_NAME"],
  "EMAIL"             => $arUser["EMAIL"],
  "LOGIN"			  => $arUser["LOGIN"]
    );

CEvent::Send("CONFIRM_OPTUSER", "s1", $arEventFields, "Y", 947);
endif;?>
<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>