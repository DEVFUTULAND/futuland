<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;

$aMenuLinksExt = $APPLICATION->IncludeComponent(
    "bitrix:menu.sections",
    "",
    array(
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "DEPTH_LEVEL" => "1",
        "IBLOCK_ID" => C_IB_PRODUCTS, // ID вашего инфоблока,по разделам которого хотите построить меню
        "IBLOCK_TYPE" => "catalog_futuland", //тип инфоблока
        "IS_SEF" => "Y",
        "SECTION_URL" => ""
    ),
    false
);
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);?>