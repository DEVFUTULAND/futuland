<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Цифровой бутик FUTULAND");
?>Спешим сообщить вам радостную новость: после нашего долгого ремонта, у вас снова есть возможность посетить наш магазин, где можно не только приобрести представленную у нас продукцию популярных брендов Xiaomi и Ninebot, но и увидеть ее поближе, чтобы убедиться в её несравнимом качестве!<br>
 <br>
 Специально для наших покупателей мы открыли магазин, чтобы все желающие могли быстро и с комфортом увидеть воочию всю невероятную и огромную товарную линейку Xiaomi и электротранспорта Ninebot.<br>
 <br>
<h2>Что продается в нашем магазине Футуленд?</h2>
 В торговом зале выставлен широкий ассортимент популярных товарных позиций, которые также представлены в онлайн-каталоге нашего магазина, но при этом их можно трогать, мерить и видеть вживую. У нас вы можете купить такие бренды, как Xiaomi, Segway by Ninebot, Mijia, Amazfit, Huami, ZMI, Huawei Honor и многие другие. А оплачивать покупки можно как безналичными, так и наличными деньгами.<br>
 <br>
 <img src="https://futuland.ru/upload/medialibrary/65f/futuland_shop-_16_.jpg"><br>
 <br>
 Но это еще не всё! Все желающие могут лично ознакомится с представленной продукцией, получить подробную консультацию о каждой интересующей их позиции от наших квалифицированных специалистов, а также испробовать товар в деле! <br>
 <br>
 Наши консультанты покажут вам, как балансировать на мини-сигвеях, научат настраивать приложение для системы умного дома, объяснят, как пользоваться теми или иными чудо-гаджетами, а также расскажут вам о различных новинках, которых вы себе даже не могли представить.<br>
 <br>
 <img src="https://futuland.ru/upload/medialibrary/828/futuland_shop-_2_.jpg"><br>
 <br>
 <br>
<h2>Где расположен магазин Футуленд и как его найти?</h2>
 Магазин расположился в Москве, неподалеку от Павелецкого вокзала, ул. Зацепа, 21.&nbsp;Вам необходимо сесть в последний вагон из центра и доехать до станции метро "Павелецкая" (радиальная) и выйти в город. Когда вы окажетесь на поверхности, через дорогу будет находится высокое здание БЦ Павелецкая Плаза, вы проходите между ними и оказываетесь на улице Зацепа.<br>
 <br>
 Повернув направо, на перекрестке вы переходите на противоположную сторону и продолжаете идти вверх по улице. Вы не пропустите магазин, ведь прямо с дороги виднеется его большая ярко-желтая вывеска!<br>
 <br>
 <img src="https://futuland.ru/upload/medialibrary/348/IMG_1513_1_2.jpg"><br>
 <br>
 Вы заходите внутрь через большую белую дверь и тут же оказываетесь рядом с нашим магазином! <br>
 <br>
 Подробная карта и другие возможные маршруте можно посмотреть в разделе <a target="_blank" href="https://futuland.ru/contacts/">Контакты</a>.&nbsp;<br>
 <br>
 <span ame="" src="https://yandex.ru/map-widget/v1/-/CCQS5N8-" width="560" height="400" frameborder="1" allowfullscreen="true"> <br>
<h5 style="text-align: center;">Будущее близко: уже в нашем магазине!</h5>
 <?$APPLICATION->IncludeComponent(
	"bitrix:photogallery.detail.list.ex",
	".default",
	Array(
		"ADDITIONAL_SIGHTS" => array(),
		"BEHAVIOUR" => "SIMPLE",
		"CACHE_TIME" => "360000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"DATE_TIME_FORMAT" => "d.m.Y",
		"DETAIL_SLIDE_SHOW_URL" => "slide_show.php?SECTION_ID=#SECTION_ID#&ELEMENT_ID=#ELEMENT_ID#",
		"DETAIL_URL" => "detail.php?SECTION_ID=#SECTION_ID#&ELEMENT_ID=#ELEMENT_ID#",
		"DISPLAY_AS_RATING" => "rating",
		"DRAG_SORT" => "Y",
		"ELEMENT_LAST_TYPE" => "none",
		"ELEMENT_SORT_FIELD" => "SORT",
		"ELEMENT_SORT_FIELD1" => "",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER1" => "asc",
		"GROUP_PERMISSIONS" => array(0=>"1",),
		"IBLOCK_ID" => "219",
		"IBLOCK_TYPE" => "PhotoGallery",
		"MAX_VOTE" => "5",
		"NAME_TEMPLATE" => "",
		"PAGE_ELEMENTS" => "50",
		"PATH_TO_USER" => "/company/personal/user/#USER_ID#",
		"PICTURES_SIGHT" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"RATING_MAIN_TYPE" => "",
		"SEARCH_URL" => "search.php",
		"SECTION_ID" => "1864",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_COMMENTS" => "N",
		"SHOW_LOGIN" => "Y",
		"SHOW_PAGE_NAVIGATION" => "bottom",
		"SHOW_RATING" => "N",
		"SHOW_SHOWS" => "N",
		"THUMBNAIL_SIZE" => "200",
		"USE_COMMENTS" => "N",
		"USE_DESC_PAGE" => "Y",
		"USE_PERMISSIONS" => "N",
		"VOTE_NAMES" => array(0=>"1",1=>"2",2=>"3",3=>"4",4=>"5",5=>"",)
	)
);?><br>
 <br>
 </span><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>