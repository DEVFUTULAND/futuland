<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?>
    <style>
        p {font-size: 1.6rem;
        line-height: 1.7; }
    </style>
    <p>Группа компаний FUTULAND – оптовый поставщик продукции техники Xiaomi и суббрендов, а также электротранспорта Ninebot by Segway.</p>
    <p>Наша миссия - сделать новинки электроники доступными для каждого. На данный момент ГК «FUTULAND» является официальным дистрибьютором ряда крупных производителей электроники, а так же владельцем собственного бренда «Hili».</p>

    <h2 class="mb2r mt3r">Наши преимущества</h2>
    <table class="table mb3r" border="1" cellpadding="1" cellspacing="1">
        <tr>
            <th style="background-color: #ffd91f; color: #000000; text-align: center;">Для розницы</th>
            <th style="background-color: #ffd91f; color: #000000; text-align: center;">Для оптовиков</th>
        </tr>
        <tr>
            <td>Широкий ассортимент товара</td>
            <td>Закупаем продукцию напрямую на заводах Китая</td>
        </tr>
        <tr>
            <td>Выгодные цены</td>
            <td>Одни из первых поставляем новинки на российский рынок</td>
        </tr>
        <tr>
            <td>Долгосрочная гарантия</td>
            <td>Наши цены всегда самые низкие</td>
        </tr>
        <tr>
            <td>Приятные бонусы и скидки</td>
            <td>Каждый товар сертифицирован и имеет гарантию</td>
        </tr>
        <tr>
            <td>Доставка по РФ</td>
            <td>Программа лояльности для новых и постоянных клиентов</td>
        </tr>
        <tr>
            <td>Поддержка профессионального сервисного центра</td>
            <td>Возможность оформить предзаказ</td>
        </tr>
    </table>

    <p>
        Вся линейка представленной у нас продукции популярных брендов Xiaomi и Ninebot размещена в нашем шоуруме, который находится в центре Москвы – м. Павелецкая, Зацепа 21. Здесь вы сможете ознакомиться с товарами поближе, убедиться в их качестве и получить полную консультацию у наших специалистов.
    </p>
    <p class="bold">На нашем сайте вы можете приобрести товар по оптовым и розничным ценам.</p>

    <p>Чтобы приобрести товар по оптовым ценам зарегистрируйтесь на сайте и пройдите модерацию.</p>
    <p>Если вы хотите совершить заказ по розничной цене, оформите заявку через сайт, закажите звонок или сделайте заказ в шоуруме.</p>

    <h2 class="mb2r mt3r">В нашем каталоге представлены товары</h2>
    <table class="table mb3r" border="1" cellpadding="1" cellspacing="1">
        <tr>
            <th style="background-color: #ffd91f; color: #000000; text-align: center;">Техника Xiaomi и суббрендов</th>
            <th style="background-color: #ffd91f; color: #000000; text-align: center;">Электротранспорт Xiaomi и Ninebot</th>
        </tr>
        <tr>
            <td>«Умный дом»</td>
            <td>Сигвеи</td>
        </tr>
        <tr>
            <td>Товары для дома</td>
            <td>Мини-сигвей</td>
        </tr>
        <tr>
            <td>Аудиотехника</td>
            <td>Электросамокаты</td>
        </tr>
        <tr>
            <td>Умные часы и фитнес-трекеры</td>
            <td>Электроролики</td>
        </tr>
        <tr>
            <td>Чемоданы и рюкзаки</td>
            <td>Электроскейт</td>
        </tr>
        <tr>
            <td>Автоаксессуары</td>
            <td>Гироскутеры</td>
        </tr>
        <tr>
            <td>Детские товары</td>
            <td>Электровелосипеды</td>
        </tr>
        <tr>
            <td>Зарядные устройства и роутеры</td>
            <td>Электроскутеры</td>
        </tr>
        <tr>
            <td>Телевизоры и ТВ оборудование</td>
            <td>Аксессуары</td>
        </tr>
        <tr>
            <td>Фото и видео техника</td>
            <td>Запчасти</td>
        </tr>
    </table>

    <p>
        <span class="bold">Сделать свой дом умным не сложно!</span> Просто подключите датчики и устройства «умного дома» Xiaomi, объедините их в одну систему и управляйте всем домашним оборудованием с помощью мобильного приложения. Xiaomi делает все, чтобы наши дома стали настоящими неприступными крепостями с внутренней высокотехнологичной экосистемой, для которых не нужны особые условия ремонта!
    </p>
    <p>
        Что касается элекротранспорта, в нашем магазине представлены летние и зимние модели, внедорожники, гироскутеры для детей, самокаты дальнего и ближнего хода. Любые расцветки на ваш вкус, дополнительные услуги, такие как пыле и влагозащита, продление гарантии и пр. На данный момент, <span class="bold">это новое веяние не оставила никого равнодушным</span> и каждый в ней нашел то, что ему нравиться, - будь то электросамокат или электроролики.
    </p>
    <p>
        <span class="bold">Приобретайте любую технику и электротранспорт по выгодным ценам на FUTULAND.RU</span>. Добавьте товар в корзину в один клик и оформите заказ буквально за пару минут. Остальную работу сделают курьеры, операторы и служба доставки. Попробуйте сами и узнайте, насколько это удобно и выгодно.
    </p>
    <p class="social-img-wrap">
        Подписывайтесь на нас в социальных сетях! Выкладываем самую актуальную информацию, статьи, новости и заманчивые предложения здесь:
        <a href="https://zen.yandex.ru/futuland" target="_blank" title="dzen" rel="nofollow"><img src="/local/templates/futuland/images/social/dzen.jpg" alt="dzen" class="bx-lazyload-success"></a>
        <a href="https://www.instagram.com/futuland/" target="_blank" rel="nofollow" title="Instagram"><img src="/local/templates/futuland/images/social/instagram.jpg" alt="instagram" class="bx-lazyload-success"></a>
        <a href="https://vk.com/futulandru" target="_blank" rel="nofollow" title="Вконтакте"><img src="/local/templates/futuland/images/social/vk.jpg" alt="vk" class="bx-lazyload-success"></a>
        <a href="https://ok.ru/dk?st.cmd=anonymGroupForum&st.gid=55997628612626" target="_blank" rel="nofollow" title="OK"><img src="/local/templates/futuland/images/social/ok.jpg" alt="ok" class="bx-lazyload-success"></a>
        <a href="https://twitter.com/gorkomps" target="_blank" rel="nofollow" title="Twitter"><img src="/local/templates/futuland/images/social/twitter.jpg" alt="twitter" class="bx-lazyload-success"></a>
    </p>
    <p>Приятных покупок!</p>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>