<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Карта сайта FUTULAND.RU");
?>

<?$APPLICATION->IncludeComponent("bitrix:main.map", "fmap", Array(
	"LEVEL" => "10",	// Максимальный уровень вложенности (0 - без вложенности)
		"COL_NUM" => "2",	// Количество колонок
		"SHOW_DESCRIPTION" => "N",	// Показывать описания
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "86400",	// Время кеширования (сек.)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>