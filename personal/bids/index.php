<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои заявки");
use FUTU\Fwholesalers,Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss("/bitrix/css/main/bootstrap_v4/bootstrap.css");
Asset::getInstance()->addJs( "/local/lib/npm/node_modules/axios/dist/axios.min.js");

$arBids = Fwholesalers::getUserBids($USER->GetID());
?>
<style>
.modal-mask {margin-top:1rem;border:1px solid lightgray;border-radius: 6px;}
.modal-mask table a{color:var(--bl)}
.modal-mask table a:hover{color:var(--primary2)}
#arBids .appVueAxios button#show-modal:focus{background-color:var(--primary2)}

</style>

<div id="arBids" class="row">
    <table class="table ">
        <thead class="thead-light">
            <tr>
                <th scope="col" class="w-25">ID</th>
                <th scope="col" class="w-25">Дата создания</th>
                <th scope="col" class="w-50">Список товаров</th>
            </tr>
        </thead>
        <tbody id="tableBody">
        <?foreach($arBids as $k=>$v)
        {?>
            <tr id="<?=$v['ID']?>">
                <td><?=$v['ID']?></td>
                <td><?=$v['DATE_CREATE']?></td>
                <td>
                    <div class="appVueAxios" v-click-outside="outside">
                        <button id="show-modal" class="btn btn-primary" @click="click_page('<?=$v['ID']?>')">Показать</button>
                        <modal v-if="showModal" @close="showModal = false" ></modal>
                    </div>
                </td>
            </tr>
        <?}?>
        </tbody>
    </table>
</div>
<!-- template for the modal component -->
<script type="text/x-template" id="modal-template">
    <transition name="modal">
        <div class="modal-mask">
            <div class="modal-wrapper" @click="$emit('close')">
                <div class="modal-container" @click.stop="">
                    <button class="modal-default-button" @click="$emit('close')"><i class="fa fa-times"></i>
                    </button>
                    <div class="modal-body">
                        <div name="body" >
                            <div v-html="info">{{ info }}</div>
                            <table id="products" class="table">
                                <tbody>
                                    <tr v-for="product in products">
                                        <td><a target="_blank" :href="product.DETAIL_PAGE_URL" >{{ product.NAME }}</a></td>
                                        <td>{{ product.COUNT }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition>
</script>
<script>
    var arBids = <?=CUtil::PhpToJSObject($arBids)?>;
    console.log(arBids);
    let curProdId = '';
    // modal component
    BX.Vue.component('modal', {
        template: '#modal-template',
        data: function () {
            return {
                info: 'Загрузка...',
                products: ''
            }
        },
        created(){
            axios
                .get('/wsb/bids.php', {params: {
                    type: "getUserBidsProducts",
                    arr: curProdId
                }})
                .then(response => (
                    this.info = null,
                    this.products = response.data,
                        console.log(response.data)
                ))
                .then(function (response) {
                    console.log(response.data);
                })
                .catch(error => {
                    console.log(error)
                    this.errored = true
                })
                .finally(() => this.loading = false)
        }
    });
    // button modal
    let elements = document.getElementsByClassName('appVueAxios')
    for(let el of elements){
        console.log(el);
        BX.Vue.create({
            el: el,
            data: {
                showModal: false
            },
            methods : {
                click_page   : function ( e ) {
                    curProdId = arBids[e].GOODS;
                    this.showModal = true;
                },
                outside: function(e) {
                    this.clickOutside += 1;
                    //this.showModal: false;
                    //console.log(el);
                    //el.lastChild.hide();
                    this.showModal = false;
                    //console.log('clicked outside!')
                }
            },
            directives: {
                'click-outside': {
                    bind: function(el, binding, vNode) {
                        // Provided expression must evaluate to a function.
                        if (typeof binding.value !== 'function') {
                            const compName = vNode.context.name
                            let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`
                            if (compName) { warn += `Found in component '${compName}'` }

                            console.warn(warn)
                        }
                        // Define Handler and cache it on the element
                        const bubble = binding.modifiers.bubble
                        const handler = (e) => {
                            if (bubble || (!el.contains(e.target) && el !== e.target)) {
                                binding.value(e)
                            }
                        }
                        el.__vueClickOutside__ = handler

                        // add Event Listeners
                        document.addEventListener('click', handler)
                    },

                    unbind: function(el, binding) {
                        // Remove Event Listeners
                        document.removeEventListener('click', el.__vueClickOutside__)
                        el.__vueClickOutside__ = null

                    }
                }
            }
        });
    }
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>