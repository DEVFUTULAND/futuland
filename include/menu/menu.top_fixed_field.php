<?//global $arTheme;?>
<?/*$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_fixed_field", 
	array(
		"COMPONENT_TEMPLATE" => "top_fixed_field",
		"MENU_CACHE_TIME" => "3600000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"ALLOW_MULTI_SELECT" => "Y",
		"ROOT_MENU_TYPE" => "top_catalog_fixed",
		"CHILD_MENU_TYPE" => "left",
		"CACHE_SELECTED_ITEMS" => "N",
		"USE_EXT" => "Y"
	),
	false
);*/?>
<ul class="nav nav-pills responsive-menu visible-xs" id="mainMenuF">
    <li class=" ">
        <a class="" href="/catalog/avtoaksessuary/" title="Автоаксессуары">Автоаксессуары</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/audiotekhnika/" title="Аудиотехника">Аудиотехника</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/detskie_tovary/" title="Детские товары">Детские товары</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/zaryadnye_ustroystva/" title="Зарядные устройства">Зарядные устройства</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/odezhda_i_obuv/" title="Одежда и обувь">Одежда и обувь</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/planshety_noutbuki_routery/" title="Планшеты, ноутбуки, роутеры">Планшеты, ноутбуки, роутеры</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/smartfony/" title="Смартфоны">Смартфоны</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/televizory_i_tv_oborudovanie/" title="Телевизоры и ТВ оборудование">Телевизоры и ТВ оборудование</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/tovary_dlya_doma/" title="Товары для дома">Товары для дома</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/umnyy_dom/" title="Умный дом">Умный дом</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/foto_i_video_tekhnika/" title="Фото и видео техника">Фото и видео техника</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/chasy_i_braslety/" title="Часы и браслеты">Часы и браслеты</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/chemodany_i_ryukzaki/" title="Чемоданы и рюкзаки">Чемоданы и рюкзаки</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/elektrotransport/" title="Электротранспорт">Электротранспорт</a>
    </li>
    <li class=" ">
        <a class="" href="/catalog/rasprodazha/" title="Распродажа">Распродажа</a>
    </li>
</ul>
