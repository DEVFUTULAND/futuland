<h3 class="title_block lg">Xiaomi Mi и Segway Ninebot - большой ассортимент цифровой техники и электротранспорта</h3>
<p>
	 Мы предлагаем покупателю широкий ассортимент цифровой техники, электротранспорта. В нашем магазине представлены товары марки <b>Xiaomi MI</b> и&nbsp;<b>Segway Ninebot</b>, от робот-пылесоса до технологий "умного" дома. Хотите обновить TV или купить одну из последних моделей умных часов марки&nbsp;<a href="https://futuland.ru/catalog/">Xiaomi MI</a>? В разделе электротранспорта вы сможете купить&nbsp;моноколеса, сигвеи, электроролики, электросамокаты марки <a href="https://futuland.ru/catalog/elektrotransport/">Segway Ninebot</a>.
</p>
 <a href="<?=SITE_DIR?>company/" class="btn btn-default">Подробнее</a>