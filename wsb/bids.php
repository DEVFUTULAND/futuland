<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use FUTU\Fwholesalers;

define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define('NO_AGENT_CHECK', true);
define("STATISTIC_SKIP_ACTIVITY_CHECK", true);

if (isset($_REQUEST["type"]) && $_REQUEST["type"] == "crm")
{
    define("ADMIN_SECTION", true);
}

if($type=="sale")
{
    if ($mode == 'checkauth')
    {
        if (
            (COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
            && (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
            && !defined("BX_SESSION_ID_CHANGE")
        ) {
            echo "failure\n", "Включена смена идентификатора сессий. В файле подключения компонента обмена, до подключения пролога определите константу BX_SESSION_ID_CHANGE: define('BX_SESSION_ID_CHANGE', false);";
        } else {
            echo "success".PHP_EOL.session_name().PHP_EOL.session_id().PHP_EOL.bitrix_sessid_get();

            COption::SetOptionString("sale", "export_session_name_" . $curPage, session_name());
            COption::SetOptionString("sale", "export_session_id_" . $curPage, session_id());
        }
    }
    elseif ($mode == 'init')
    {
        echo "zip=yes".PHP_EOL."file_limit=200400".PHP_EOL.bitrix_sessid_get().PHP_EOL."version=3.01";
    }
    elseif ($mode == 'query')
    {
        $xml = Fwholesalers::makeXml();
        if($xml == 'empty'){
            echo "Нет невыгруженных заявок";
        } else echo $xml;
    }
    elseif ($mode == 'success')
    {
        echo "success".PHP_EOL.session_name().PHP_EOL.session_id().PHP_EOL.bitrix_sessid_get();
        dfile(date("d-m-Y H:i:s")."success-setBidsStatusY-".serialize($_SESSION['RUNTIME_BIDS']));
    }
    elseif ($mode == 'file')
    {
        echo "success".PHP_EOL;
    }
    elseif ($mode == 'import')
    {
        echo "success".PHP_EOL;
    }
    else {
        $APPLICATION->RestartBuffer();
        $uid = $USER->GetID();
        echo "failure".PHP_EOL."ERROR in mode: uid=" . $uid .", mode=".$mode;
    }
    dfile(date("d-m-Y H:i:s").PHP_EOL."type -sale request=".PHP_EOL.serialize($_REQUEST));
}
elseif($type=="catalog")
{
    echo "success".PHP_EOL.session_name().PHP_EOL.session_id().PHP_EOL.bitrix_sessid_get();
}
elseif($type=="setBid")
{
    $arr = json_decode($_REQUEST['arr'],true);
    $bid = Fwholesalers::setBid($arr);
    header('Content-type: application/json');
    echo json_encode($bid);
}
elseif($type=="getUserBidsProducts")
{
    $arr = json_decode($_REQUEST['arr'],true);
    $products = Fwholesalers::getUserBidsProducts($arr);
    header('Content-type: application/json');
    echo json_encode($products);
}
else
{
    $APPLICATION->RestartBuffer();
    echo "failure".PHP_EOL."ERROR, type=".serialize($_REQUEST);
}
