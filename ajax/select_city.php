<?
define("STATISTIC_SKIP_ACTIVITY_CHECK", "true");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if($_REQUEST['setLocManual'] == "Y"){
    $_SESSION['FUTU']['REGION']['LOCATION'] = $_REQUEST;
    $_SESSION['FUTU']['REGION']['METHOD'] = "manual";
    $_SESSION['FUTU']['REGION']['BX_LOC_CODE'] = $_REQUEST['CODE'];
}else{
    global $APPLICATION;
    $APPLICATION->ShowHead();
    ?>
    <a href="#" class="close jqmClose"><i></i></a>
    <?$APPLICATION->IncludeComponent(
        "bitrix:sale.location.selector.search",
        "f_geoloc",
        Array(
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CODE" => "",
            "FILTER_BY_SITE" => "Y",
            "FILTER_SITE_ID" => "s1",
            "ID" => "92",
            "INITIALIZE_BY_GLOBAL_EVENT" => "",
            "INPUT_NAME" => "LOCATION",
            "JS_CALLBACK" => "",
            "JS_CONTROL_GLOBAL_ID" => "",
            "PROVIDE_LINK_BY" => "id",
            "SHOW_DEFAULT_LOCATIONS" => "Y",
            "SUPPRESS_ERRORS" => "N"
        )
    );
}


