<?
$bUseMap = CNext::GetFrontParametrValue('CONTACTS_USE_MAP', SITE_ID) != 'N';
$bUseFeedback = CNext::GetFrontParametrValue('CONTACTS_USE_FEEDBACK', SITE_ID) != 'N';
?>

<?CNext::ShowPageType('page_title');?>

<div class="contacts maxwidth-theme" itemscope itemtype="http://schema.org/Organization">
	<div class="row">
		<div class="<?=($bUseMap ? 'col-md-4 col-sm-5 col-xs-12 col-md-push-8 col-sm-push-7' : 'col-md-12')?>">
            <div class="contacts-wrap">
                <div class="contact-title">
                    <span class="mobile">Показать контактную информацию</span>
                    <span class="desktop">Контактная информация</span>
                </div>
                <div class="contact-info">
                    <div>
                        <span itemprop="description"><?$APPLICATION->IncludeFile(SITE_DIR."include/contacts-about.php", Array(), Array("MODE" => "html", "NAME" => "Contacts about"));?></span>
                    </div>
                    <table>
                        <tbody>
                            <?CNext::showContactAddr('Адрес:', false);?>
                            <?CNext::showContactPhones('Телефон:', false);?>
                            <?CNext::showContactEmail('E-mail:', false);?>
                            <?CNext::showContactSchedule('Режим работы:', false);?>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
		<?if($bUseMap):?>
			<div class="col-md-8 col-sm-7 col-xs-12 col-md-pull-4 col-sm-pull-5">
				<?$APPLICATION->IncludeFile("/include/contacts-site-map.php", Array(), Array("MODE" => "html", "TEMPLATE" => "include_area.php", "NAME" => "Карта"));?>
			</div>
		<?endif;?>
	</div>
    <div class="row mt5r mb2r">
        <?$APPLICATION->IncludeFile("/include/contacts-how-to-get.php", Array(), Array("MODE" => "html", "TEMPLATE" => "include_area.php", "NAME" => "Как добраться"));?>
    </div>
	<?//hidden text for validate microdata?>
	<?/*<div class="hidden">
		<?global $arSite;?>
		<span itemprop="name"><?=$arSite["NAME"];?></span>
	</div>*/?>
</div>
<?if($bUseFeedback):?>
	<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("contacts-form-block");?>
	<?global $arTheme;?>
	<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "inline",
		Array(
			"WEB_FORM_ID" => "3",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"USE_EXTENDED_ERRORS" => "Y",
			"SEF_MODE" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600000",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "?send=ok",
			"SHOW_LICENCE" => $arTheme["SHOW_LICENCE"]["VALUE"],
			"HIDDEN_CAPTCHA" => CNext::GetFrontParametrValue('HIDDEN_CAPTCHA'),
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"VARIABLE_ALIASES" => Array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID"
			)
		)
	);?>
	<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("contacts-form-block", "");?>
<?endif;?>