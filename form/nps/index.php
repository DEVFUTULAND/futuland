<meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
    <link rel="stylesheet" href="https://fast.ulmart.ru/ns/promokv/ythx/style/fonts.css">
    <link rel="stylesheet" href="/form/nps/main.css">
	<link rel="stylesheet" href="https://fast.ulmart.ru/ns/promokv/ythx/style/main.css">
    <script src="https://code.jquery.com/jquery.min.js"> </script>

<body class="t-dev" cz-shortcut-listen="true">

    <div class="content">
      <div class="content__wrapper">
		  <div class="content__header"><a href="https://futuland.ru" target="_blank"><img src="https://futuland.ru/local/img/futuland_nps.jpg" alt="Futuland.ru"></a></div>
        <div class="content__body">
          <div class="content__title">
            Мы очень рады, <br>
            что вы&nbsp;оцениваете нашу работу так&nbsp;высоко.
          </div><a id="y-link" href="https://market.yandex.ru/shop/391564/reviews/add" target="_blank" class="btn" onclick="yaCounter50513497.reachGoal('nps_market');"><img src="https://fast.ulmart.ru/ns/promokv/ythx/i/logo-yandeks-market.png" alt="Перейти в Ямаркет">Оставьте отзыв на Яндекс Маркете</a>
        </div>
        <div class="content__footer">
          <div class="social">
            <div class="social__title">ПРИСОЕДИНЯЙТЕСЬ</div>
            <div class="social__items">
			<a rel="nofollow" href="http://instagram.com/futuland/" class="social-item" target="_blank"><i class="icon icon--ig"></i></a>
			<a rel="nofollow" href="http://vk.com/futulandru" class="social-item" target="_blank"><i class="icon icon--vk"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50513497 = new Ya.Metrika2({
                    id:50513497,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50513497" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->