<?
//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Спасибо за покупку в Futuland");
?><head>
<title>Спасибо за покупку в Futuland</title>
</head>
<div align="center">
	<table cellpadding="0" cellspacing="0" style="background-color:#f6f6f6;border:0 none;border-collapse:collapse;border-spacing:0;width:100%">
	<tbody>
	<tr>
		<td align="center" style="border:0 none;border-collapse:collapse;border-spacing:0;padding-bottom:50px">
			<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
				<table cellpadding="0" cellspacing="0" align="center" width="560" style="margin:0 auto;border:0 none;border-collapse:collapse;border-spacing:0;font-size:11px;margin:0 auto">
				<tbody>
				<tr>
					<td align="center" style="border:0 none;border-collapse:collapse;border-spacing:0;font-family:Arial,Helvetica,sans-serif;padding:10px 0">
 <!--a href="https://futuland.ru/form/nps/nps.php" style="color:#999999;font-family:Arial,Helvetica,sans-serif;font-size:11px;text-decoration:none" rel=" noopener noreferrer" data-snippet-id="95a5fd40-4434-c0e3-b7b7-88d8b377d39" target="_blank">Посмотреть письмо в браузере</a-->
					</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
				<table cellpadding="0" cellspacing="0" align="center" width="600" style="margin:0 auto;border:0 none;border-collapse:collapse;border-spacing:0">
				<tbody>
				<tr>
					<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding:8px 0 18px">
						<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
							<table cellpadding="0" cellspacing="0" width="580" style="border:0 none;border-collapse:collapse;border-spacing:0">
							<tbody>
							<tr>
								<td align="center" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru" target="_blank" rel=" noopener noreferrer"><img width="350" src="https://futuland.ru/local/img/futuland_nps_grey.jpeg" height="60"></a>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td align="center" style="border:0 none;border-collapse:collapse;border-spacing:0">
						<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
							<table cellpadding="0" cellspacing="0" style="border:0 none;border-collapse:collapse;border-spacing:0;width:100%" align="center">
							<tbody>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;color:#fff;font-size:14px" align="center">
 <a href="https://futuland.ru" target="_blank" rel=" noopener noreferrer"><img width="600" src="https://futuland.ru/local/img/nps/header.jpg" alt=""></a>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
				<table cellpadding="0" cellspacing="0" align="center" width="600" style="margin:0 auto;background-color:#ffffff;border:0 none;border-collapse:collapse;border-spacing:0">
				<tbody>
				<tr>
					<td align="center" style="border:0 none;border-collapse:collapse;border-spacing:0;padding:35px 0 20px">
						<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
							<table cellpadding="0" cellspacing="0" align="center" width="560" style="border:0 none;border-collapse:collapse;border-spacing:0">
							</table>
							<table cellpadding="0" cellspacing="0" align="center" width="560" style="border:0 none;border-collapse:collapse;border-spacing:0">
							<tbody>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;color:#25232a;font-size:26px;font-weight:bold;padding-bottom:25px;text-align:center">
									<div style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
										 Спасибо за покупку в&nbsp;FUTULAND
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;color:#25232a;font-size:14px;text-align:center">
									<div style="color:#25232a;font-family:Arial,Helvetica,sans-serif;line-height:1.5">
										 Нам очень важна обратная связь о нашей работе — она позволяет взглянуть на компанию со стороны и улучшить качество сервиса для наших дорогих клиентов, для вас.
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;color:#25232a;font-size:14px;text-align:center">
									<div style="color:#25232a;font-family:Arial,Helvetica,sans-serif;line-height:1.5">
										 Пожалуйста, оцените, с какой вероятностью вы порекомендуете Futuland своим друзьям и знакомым - поставьте оценку от 0 до 10
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding-top:30px">
									<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
										<table cellpadding="0" cellspacing="0" width="520" style="border:0 none;border-collapse:collapse;border-spacing:0">
										<tbody>
										<tr>
											<td align="left" style="border:0 none;border-collapse:collapse;border-left:3px solid #dc1838;border-spacing:0;color:#565454;font-size:12px;padding-left:8px">
												<div style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
													 Ни в коем случае <br>
													 не порекомендую
												</div>
											</td>
											<td align="right" style="border:0 none;border-collapse:collapse;border-right:3px solid #4c9815;border-spacing:0;color:#565454;font-size:12px;padding-right:8px">
												 Обязательно<br>
												 порекомендую
											</td>
										</tr>
										</tbody>
										</table>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding-bottom:20px;padding-top:20px">
									<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
										<table cellpadding="0" cellspacing="0" width="100%" style="border:0 none;border-collapse:collapse;border-spacing:0">
										<tbody>
										<tr>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/0.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/1.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/2.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/3.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/4.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/5.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/6.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/7.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/8.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/9.png"></a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <a href="https://futuland.ru/form/nps/" rel=" noopener noreferrer" target="_blank"><img src="https://futuland.ru/local/img/nps/10.png"></a>
											</td>
										</tr>
										<tr>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">0</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">1</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">2</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">3</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">4</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">5</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">6</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">7</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/red.php?utm_source=nps_form" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">8</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">9</a>
											</td>
											<td align="center" width="50" style="border:0 none;border-collapse:collapse;border-spacing:0;color:#000000;font-size:12px">
 <a href="https://futuland.ru/form/nps/" style="color:#000000;text-decoration:none" rel=" noopener noreferrer" target="_blank">10</a>
											</td>
										</tr>
										</tbody>
										</table>
									</div>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
				<table cellpadding="0" cellspacing="0" align="center" width="600" style="margin:0 auto;border:0 none;border-collapse:collapse;border-spacing:0">
				<tbody>
				<tr>
					<td style="border:0 none;border-collapse:collapse;border-spacing:0">
						 &nbsp;
					</td>
				</tr>
				</tbody>
				</table>
			</div>
			<div align="center" style="font-family:Arial,Helvetica,sans-serif;line-height:1.2">
				<table cellpadding="0" cellspacing="0" width="600" style="background-color:#ffffff;border:0 none;border-collapse:collapse;border-spacing:0;color:#808080;font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify">
				<tbody>
				<tr>
					<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding:15px 0">
						<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
							<table cellpadding="0" cellspacing="0" align="center" width="580" style="margin:0 auto;border:0 none;border-collapse:collapse;border-spacing:0">
							<tbody>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0">
									<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
										<table cellpadding="0" cellspacing="0" width="100%" style="border:0 none;border-collapse:collapse;border-spacing:0">
										<tbody>
										<tr>
											<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding-top:5px">
												<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
													
													<table cellpadding="0" cellspacing="0" width="100%" align="left" style="border:0 none;border-collapse:collapse;border-spacing:0">
													<tbody>
													<tr>
														<td align="right" style="border:0 none;border-collapse:collapse;border-spacing:0;padding-right:5px;">
													<a href="https://vk.com/futulandru" rel=" noopener noreferrer" target="_blank"><img width="19" src="https://futuland.ru/local/img/nps/vk_black.png" height="19"></a>
														</td>
														<td align="left" style="border:0 none;border-collapse:collapse;border-spacing:0;padding-left:5px;">
													<a href="http://www.instagram.com/futuland/" rel=" noopener noreferrer" target="_blank"><img width="19" src="https://futuland.ru/local/img/nps/ig_black.png" height="19"></a>
														</td>
													</tr>
													</tbody>
													</table>
												</div>
											</td>
										</tr>
										</tbody>
										</table>
									</div>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td style="border:0 none;border-collapse:collapse;border-spacing:0">
						<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
							<table cellpadding="0" cellspacing="0" width="570" style="margin:0 auto;border:0 none;border-collapse:collapse;border-spacing:0;color:#808080;font-family:Arial,Helvetica,sans-serif;font-size:11px;text-align:justify">
							<tbody>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0">
									<div style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
 <br>
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding:20px 0">
									<div style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
										 Вам было отправлено данное письмо, так как вы оформляли заказ в Интернет-магазине цифровой техники FUTULAND.RU. Посмотреть <a href="https://futuland.ru/include/licenses_detail.php" rel=" noopener noreferrer" style="color: #0076c0; font-weight: bold;" target="_blank">информацию</a> о&nbsp;правилах обработки персональных данных.
									</div>
								</td>
							</tr>
							<tr>
								<td style="border:0 none;border-collapse:collapse;border-spacing:0;padding-bottom:30px">
									<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
										<table cellpadding="0" cellspacing="0" width="570" style="border:0 none;border-collapse:collapse;border-spacing:0">
										<tbody>
										<tr>
											<td style="border:0 none;border-collapse:collapse;border-spacing:0">
												<div align="center" style="color:#808080;font-family:Arial,Helvetica,sans-serif;line-height:1.4">
 <br>
												</div>
											</td>
											<td align="right" style="border:0 none;border-collapse:collapse;border-spacing:0">
 <br>
											</td>
										</tr>
										</tbody>
										</table>
									</div>
								</td>
							</tr>
							</tbody>
							</table>
						</div>
					</td>
				</tr>
				</tbody>
				</table>
			</div>
		</td>
	</tr>
	</tbody>
	</table>
</div>
<br>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50513497 = new Ya.Metrika2({
                    id:50513497,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50513497" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter --><?//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>