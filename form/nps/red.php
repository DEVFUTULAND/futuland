<head>
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
  <script src="https://code.jquery.com/jquery.min.js"> </script>
  <style>

    html, body {
      height: 100%;
    }
    body {
      margin: 0;
    }
    .logo {
      position: relative;
      display: block;
      margin: 0 auto 12vh;
      width: 100%;
      max-width: 490px;
      height: 160px;
      text-align: center;
      
    }
    .logo img {
      display: block;
      margin: 0 auto;
      max-width: 100%;

      padding-top: 40px;
    }
    .content {

      padding: 0 10px;


      text-align: center;
      font-family: Arial, Helvetica, sans-serif;
    }
    .content__title {
      font-size: 24px;

      margin-bottom: 10vh;



      color: #000000;


    }
    .content__desc {
      font-size: 36px;

      font-weight: 700;


      line-height: 1.3;
      color: #1e2a18;

    }

    .show-md {
      display: none;
    }

    @media (max-width: 1024px) {
      .content__title {

        font-size: 2.5vmax;
      }
      .content__desc  {
        font-size: 3.2vmax;
      }
      }
    @media (min-width:768px) {
      .show-md {
        display: block;
      }
    }
    .debug {
      font-size: 20px;
      padding: 20px;
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      z-index: 9;

      background-color: Gainsboro ;
    }
    .d-type {
      color: salmon;
      font-weight: 700;
    }
    .d-search {
      color: Indigo;
    }
  </style>

</head>
<body>

  <!-- logo -->
  <a href="https://www.futuland.ru/" class="logo">
    <img src="https://futuland.ru/local/img/futuland_nps.jpg" alt="">
  </a>
  <!-- ./ -->

  <!-- body -->
    <div class="content">
      <div class="content__title">
        Нам очень жаль, что мы не&nbsp;отработали на&nbsp;отлично.
      </div>
      <div class="content__desc">
        Сейчас вы будете перенаправлены <br class="show-md">
        на&nbsp;страницу опроса — расскажите, <br class="show-md">
        что нам нужно улучшить.
      </div>
    </div>
  <!-- ./ -->

  <script>
        $( document ).ready(function() {
          var url = 'https://ru.surveymonkey.com/r/DTXPJGG';
             setTimeout(function() {window.location.href = url}, 2000 )
        });
  </script>
  	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50513497 = new Ya.Metrika2({
                    id:50513497,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50513497" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
